import os
import sys

# from raygun4py.middleware import flask as flask_raygun

PYTHON_VERSION = sys.version_info[0]
if PYTHON_VERSION == 3:
    import urllib.parse
else:
    import urlparse

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    APP_NAME = os.environ.get('APP_NAME', 'Corona')

    if os.environ.get('SECRET_KEY'):
        SECRET_KEY = os.environ.get('SECRET_KEY')
    else:
        SECRET_KEY = 'SECRET_KEY_ENV_VAR_NOT_SET'
        print('SECRET KEY ENV VAR NOT SET! SHOULD NOT SEE IN PRODUCTION')
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    """Database"""
    POSTGRES_URL = os.environ.get('POSTGRES_URL', 'localhost')
    POSTGRES_USER = os.environ.get('POSTGRES_USER', 'postgres')
    POSTGRES_PW = os.environ.get('POSTGRES_PASSWORD', 'password')
    POSTGRES_PORT = os.environ.get('POSTGRES_PORT', '5432')
    POSTGRES_DB = os.environ.get('POSTGRES_DB', 'db')
    """Upload files"""
    UPLOAD_FILE = os.path.join(basedir, 'app/static/uploads')
    """Pagination"""
    RESULTS_PER_PAGE = 5

    # Images
    # IMAGES_URL = '/static'
    # IMAGES_PATH = 'static/dist/img/'
    # IMAGES_MAX_AGE = '3600'

    # Cache
    CACHE_TYPE = "simple" # Flask-Caching related configs
    CACHE_DEFAULT_TIMEOUT = 300

    # Recapcha
    RECAPTCHA_USE_SSL = os.environ.get('RECAPTCHA_USE_SSL', False)
    RECAPTCHA_PUBLIC_KEY = os.environ.get(
        'RECAPTCHA_PUBLIC_KEY', '6Lfmy-YUAAAAAM6GbarMt37Jg3oKXHaPNe1t2RPp')
    RECAPTCHA_PRIVATE_KEY = os.environ.get(
        'RECAPTCHA_PRIVATE_KEY', '6Lfmy-YUAAAAAFhKDLbJ5g6-beGDAuiqnug-abcj')
    RECAPTCHA_OPTIONS = {'theme': 'white'}

    # Email
    MAIL_SERVER = os.environ.get('MAIL_SERVER', 'smtp.sendgrid.net')
    MAIL_PORT = os.environ.get('MAIL_PORT', 587)
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', True)
    MAIL_USE_SSL = os.environ.get('MAIL_USE_SSL', False)
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER')

    # Pagination
    RESULTS_PER_PAGE = 5

    # Analytics
    GOOGLE_ANALYTICS_ID = os.environ.get('GOOGLE_ANALYTICS_ID', 'auto')
    SEGMENT_API_KEY = os.environ.get('SEGMENT_API_KEY', 'auto')

    # ElasticSearch
    ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL',
                                       'http://corona-elasticsearch:9200')

    # Parse the ELASTICSEARCH_URL to set Elasticsearch config variables
    if PYTHON_VERSION == 3:
        urllib.parse.uses_netloc.append('http')
        url = urllib.parse.urlparse(ELASTICSEARCH_URL)
    else:
        urlparse.uses_netloc.append('http')
        url = urlparse.urlparse(ELASTICSEARCH_URL)

    # Issue Tracker
    SENTRY_DSN = os.environ.get('SENTRY_DSN', '')

    # Admin account
    ADMIN_PASSWORD = os.environ.get('ADMIN_PASSWORD', './Password123!@#')
    ADMIN_EMAIL = os.environ.get('ADMIN_EMAIL', 'admin@desakupemalang.id')
    EMAIL_SUBJECT_PREFIX = '[{}]'.format(APP_NAME)
    EMAIL_SENDER = '{app_name} Administrator <{email}>'.format(
        app_name=APP_NAME, email=MAIL_USERNAME)

    # Redis Configuration
    # REDIS_URL = 'http://sidekem-redis:6379'
    REDIS_URL = os.getenv('REDISTOGO_URL', 'redis://corona-redis:6379/0')

    # QUEUES = ['default']

    # Raygun Configuration
    # RAYGUN_APIKEY = os.environ.get('RAYGUN_APIKEY')

    # Parse the REDIS_URL to set RQ config variables
    if PYTHON_VERSION == 3:
        urllib.parse.uses_netloc.append('redis')
        url = urllib.parse.urlparse(REDIS_URL)
    else:
        urlparse.uses_netloc.append('redis')
        url = urlparse.urlparse(REDIS_URL)

    RQ_DEFAULT_HOST = url.hostname
    RQ_DEFAULT_PORT = url.port
    RQ_DEFAULT_PASSWORD = url.password
    RQ_DEFAULT_DB = 0

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    ASSETS_DEBUG = True
    SQLALCHEMY_DATABASE_URI = \
        'postgres+psycopg2://{user}:{pw}@{url}/{db}'.format(
            user=Config.POSTGRES_USER,
            pw=Config.POSTGRES_PW,
            url=Config.POSTGRES_URL,
            db=Config.POSTGRES_DB)

    @classmethod
    def init_app(cls, app):
        print('Aplikasi Di Mode Debug Development !!')


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = \
        'postgres+psycopg2://{user}:{pw}@{url}/{db}'.format(
            user=Config.POSTGRES_USER,
            pw=Config.POSTGRES_PW,
            url=Config.POSTGRES_URL,
            db=Config.POSTGRES_DB)
    WTF_CSRF_ENABLED = False

    @classmethod
    def init_app(cls, app):
        print('Aplikasi Di Mode Debug Testing !!')


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = \
        'postgres+psycopg2://{user}:{pw}@{url}/{db}'.format(
            user=Config.POSTGRES_USER,
            pw=Config.POSTGRES_PW,
            url=Config.POSTGRES_URL,
            db=Config.POSTGRES_DB)
    SSL_DISABLE = (os.environ.get('SSL_DISABLE', 'True') == 'True')

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)
        assert os.environ.get('SECRET_KEY'), 'SECRET_KEY IS NOT SET!'

        # flask_raygun.Provider(app, app.config['RAYGUN_APIKEY']).attach()


class HerokuConfig(ProductionConfig):
    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)

        # Handle proxy server headers
        from werkzeug.contrib.fixers import ProxyFix
        app.wsgi_app = ProxyFix(app.wsgi_app)


class UnixConfig(ProductionConfig):
    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)

        # Log to syslog
        import logging
        from logging.handlers import SysLogHandler
        syslog_handler = SysLogHandler()
        syslog_handler.setLevel(logging.WARNING)
        app.logger.addHandler(syslog_handler)


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig,
    'heroku': HerokuConfig,
    'unix': UnixConfig
}
