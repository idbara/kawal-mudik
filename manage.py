#!/usr/bin/env python
import os
import subprocess
import time

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Shell
# import redis
from redis import Redis
from rq import Connection, Worker, Queue

from app import create_app, db
from app.models import (Role, User)
from app.main.commands.import_areacode import (import_country,
                                               import_provinces,
                                               import_regencies,
                                               import_districts,
                                               import_villages)
from app.main.commands.import_core_data import (import_posko,
                                                import_rumahsakit,
                                                import_puskesmas)
from app.models import CovidPerantau
from app.main.commands.import_users import import_users_data
from config import Config

from app.scrape.run import scrape

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
migrate = Migrate(app, db)


def make_shell_context():
    return dict(app=app, db=db, User=User, Role=Role)


manager.add_command('shell', Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)


@manager.command
def import_kode_wilayah():
    print("Persiapan import data negara")
    print("Mohon tunggu .....")
    time.sleep(2)
    import_country()
    print("Import data negara telah selesai")

    print("Persiapan import data provinsi")
    print("Mohon tunggu .....")
    time.sleep(2)
    import_provinces()
    print("Import data provinsi telah selesai")

    print("Persiapan import data kabupaten")
    print("Mohon tunggu .....")
    time.sleep(2)
    import_regencies()
    print("Import data kabupaten telah selesai")

    print("Persiapan import data kecamatan")
    print("Mohon tunggu .....")
    time.sleep(2)
    import_districts()
    print("Import data kecamatan telah selesai")

    print("Persiapan import data desa")
    print("Mohon tunggu .....")
    time.sleep(2)
    import_villages()
    print("Import data desa telah selesai")


@manager.command
def import_core_data():
    print("Persiapan import data Posko")
    print("Mohon tunggu .....")
    time.sleep(1)
    import_posko()
    print("Import data posko telah selesai")

    print("Persiapan import data rumah sakit")
    print("Mohon tunggu .....")
    time.sleep(2)
    import_rumahsakit()
    print("Import data rumah sakit telah selesai")

    print("Persiapan import data puskesmas")
    print("Mohon tunggu .....")
    time.sleep(2)
    import_puskesmas()
    print("Import data puskesmas telah selesai")


@manager.command
def import_users():
    print("Persiapan import data users")
    print("Mohon tunggu .....")
    time.sleep(1)
    import_users_data()
    print("Import data users telah selesai")


@manager.command
def reindex():
    """Run to Reindex using Elasticsearch"""
    CovidPerantau.reindex()
    print("Reindex Elasticsearch selesai")


@manager.command
def test():
    """Run the unit tests."""
    import unittest

    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)


@manager.command
def recreate_db():
    """
    Recreates a local database. You probably should not use this on
    production.
    """
    db.drop_all()
    db.create_all()
    db.session.commit()


@manager.option('-n',
                '--number-users',
                default=10,
                type=int,
                help='Number of each model type to create',
                dest='number_users')
def add_fake_data(number_users):
    """
    Adds fake data to the database.
    """
    User.generate_fake(count=number_users)


@manager.command
def setup_dev():
    """Runs the set-up needed for local development."""
    setup_general()


@manager.command
def setup_prod():
    """Runs the set-up needed for production."""
    setup_general()


def setup_general():
    """Runs the set-up needed for both local development and production.
       Also sets up first admin user."""
    Role.insert_roles()
    admin_query = Role.query.filter_by(name='Administrator')
    if admin_query.first() is not None:
        if User.query.filter_by(email=Config.ADMIN_EMAIL).first() is None:
            user = User(first_name='Admin',
                        last_name='Account',
                        password=Config.ADMIN_PASSWORD,
                        confirmed=True,
                        email=Config.ADMIN_EMAIL)
            db.session.add(user)
            db.session.commit()
            print('Added administrator {}'.format(user.full_name()))


@manager.command
def run_worker():
    """Initializes a slim rq task queue."""
    listen = ['default']

    redis_url = app.config['REDIS_URL']
    conn = Redis.from_url(redis_url)

    # conn = Redis(host=app.config['REDIS_URL'],
    #             port=app.config['RQ_DEFAULT_PORT'],
    #             db=0,
    #             password=app.config['RQ_DEFAULT_PASSWORD'])

    with Connection(conn):
        worker = Worker(map(Queue, listen))
        worker.work()


@manager.command
def format():
    """Runs the yapf and isort formatters over the project."""
    isort = 'isort -rc *.py app/'
    yapf = 'yapf -r -i *.py app/'

    print('Running {}'.format(isort))
    subprocess.call(isort, shell=True)

    print('Running {}'.format(yapf))
    subprocess.call(yapf, shell=True)


@manager.command
def scrapping():
    """Runs scrapping."""
    print("Persiapan scraping")
    print("Mohon tunggu .....")
    time.sleep(1)
    print(scrape())


if __name__ == '__main__':
    manager.run()
