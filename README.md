# Desa vs Corona
[![pipeline status](http://git.puspindes.id/bowo_anakdesa/sidekem-reborn/badges/master/pipeline.svg)](http://git.puspindes.id/bowo_anakdesa/sidekem-reborn/commits/master)

Update Informasi Covid-19, Monitoring Pemudik, Lapor Sakit dan Screening mandiri

<img src="https://i.ibb.co/7Q887ys/imageedit-40-5451905118-compressor.png" class="pquote-avatar" alt="avatar">
<img src="https://i.ibb.co/L0g0NTj/imageedit-43-6957633740-compressor.png" class="pquote-avatar" alt="avatar">
<img src="https://i.ibb.co/WWypVdt/imageedit-37-6692352039-compressor.png" class="pquote-avatar" alt="avatar">

## Demo
![home](doc/homepage.png "Homepage")

![home](doc/petasebaran.png "Petasebaran")

## Setting up

##### Clone the Repository 

```
$ https://git.puspindes.id/bowo_anakdesa/covid-19
$ cd covid-19
```
## Build and Create Database

##### Build Aplication

```
$ docker-compose build
```

##### Create Database

```
$ docker-compose run --rm web sh -c "python manage.py recreate_db"
```

## Add Core Data into Database


##### Import Setup Development into Database

```
$ docker-compose run --rm web sh -c "python manage.py setup_dev" 
```

##### Import Kode Wilayah into Database

```
$ docker-compose run --rm web sh -c "python manage.py import_kode_wilayah" 
```

##### Import Core Data into Database

```
$ docker-compose run --rm web sh -c "python manage.py import_core_data"
```

##### Import Users Data into Database

```
$ docker-compose run --rm web sh -c "python manage.py import_users"
```

##### Scrapping data from infocorona.pemalangkab.go.id

```
$ docker exec corona-web python manage.py scrapping
```


## Running the app

```
$ docker-compose up
```

## Formatting code

Before you submit changes to repository, you may want to autoformat your code with `$ docker-compose run --rm web sh -c "python manage.py format"`.


## Contributing

Contributions are welcome! Please refer to our [CONTRIBUTING](./CONTRIBUTING.md) for more information.


## License
[MIT License](LICENSE.md)