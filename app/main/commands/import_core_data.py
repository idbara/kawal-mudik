from app import db
import json
import os
from app.models import (Posko, RumahSakit, Puskesmas)
from sqlalchemy import exc


def import_posko():
    """Import Data Posko to Database"""
    file = os.path.abspath('app/resources') + "/posko.json"
    json_data = open(file).read()
    json_obj = json.loads(json_data)
    for row in json_obj:
        try:
            data = Posko(name=row['name'],
                         alamat=row['alamat'],
                         telp=row['telp'])
            db.session.add(data)
            db.session.commit()
            print('Import data posko {} telah selesai.'.format(data.name))
        except exc.IntegrityError:
            print('Data posko {} sudah ada !!'.format(data.name))
            db.session.rollback()


def import_rumahsakit():
    """Import Data RumahSakit to Database"""
    file = os.path.abspath('app/resources') + "/rs.json"
    json_data = open(file).read()
    json_obj = json.loads(json_data)
    for row in json_obj:
        try:
            data = RumahSakit(name=row['name'],
                              alamat=row['alamat'],
                              telp=row['telp'])
            db.session.add(data)
            db.session.commit()
            print('Import data rumah sakit {} telah selesai.'.format(
                data.name))
        except exc.IntegrityError:
            print('Data rumah sakit {} sudah ada !!'.format(data.name))
            db.session.rollback()


def import_puskesmas():
    """Import Data Puskesmas to Database"""
    file = os.path.abspath('app/resources') + "/puskesmas.json"
    json_data = open(file).read()
    json_obj = json.loads(json_data)
    for row in json_obj:
        try:
            data = Puskesmas(name=row['name'],
                             alamat=row['alamat'],
                             telp=row['telp'])
            db.session.add(data)
            db.session.commit()
            print('Import data puskesmas {} telah selesai.'.format(data.name))
        except exc.IntegrityError:
            print('Data puskesmas {} sudah ada !!'.format(data.name))
            db.session.rollback()
