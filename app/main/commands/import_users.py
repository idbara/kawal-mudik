from app import db
import json
import os
from app.models import User
from sqlalchemy import exc


def import_users_data():
    """Import Data Users to Database"""
    file = os.path.abspath('app/resources') + "/users.json"
    json_data = open(file).read()
    json_obj = json.loads(json_data)
    for row in json_obj:
        try:
            data = User(province_id=row['province_id'],
                        regency_id=row['regency_id'],
                        district_id=row['district_id'],
                        village_id=row['village_id'],
                        first_name=row['first_name'],
                        last_name=row['last_name'],
                        email=row['email'],
                        password_hash=row['password_hash'],
                        role_id=row['role_id'],
                        file_sk=row['file_sk'])
            db.session.add(data)
            db.session.commit()
            print('Import data users dengan email {} telah selesai.'.format(
                data.email))
        except exc.IntegrityError:
            print('Data users {} sudah ada !!'.format(data.email))
            db.session.rollback()
