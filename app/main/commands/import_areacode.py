from app import db
import json
import os
from app.models import (Country, Provinces, Regencies, Districts, Villages)
from sqlalchemy import exc


def import_country():
    """Import Country into Database"""
    file = os.path.abspath('app/resources') + "/country.json"
    json_data = open(file).read()
    json_obj = json.loads(json_data)
    for row in json_obj:
        try:
            data = Country(id=row['id'], name=row['name'])
            db.session.add(data)
            db.session.commit()
            print('Import data negara {} telah selesai.'.format(data.name))
        except exc.IntegrityError:
            print('Data negara {} sudah ada !!'.format(data.name))
            db.session.rollback()


def import_provinces():
    """Import Data Province to Database"""
    file = os.path.abspath('app/resources') + "/provinces-full.json"
    json_data = open(file).read()
    json_obj = json.loads(json_data)
    for row in json_obj:
        try:
            data = Provinces(id=row['id'], name=row['name'])
            db.session.add(data)
            db.session.commit()
            print('Import data provinsi {} telah selesai.'.format(data.name))
        except exc.IntegrityError:
            print('Data provinsi {} sudah ada !!'.format(data.name))
            db.session.rollback()


def import_regencies():
    """Import Data Regency to Database"""
    file = os.path.abspath('app/resources') + "/regencies-full.json"
    json_data = open(file).read()
    json_obj = json.loads(json_data)
    for row in json_obj:
        try:
            data = Regencies(id=row['id'],
                             name=row['name'],
                             province_id=row['province_id'])
            db.session.add(data)
            db.session.commit()
            print('Import data kabupaten {} telah selesai.'.format(data.name))
        except exc.IntegrityError:
            print('Data kabupaten {} sudah ada !!'.format(data.name))
            db.session.rollback()


def import_districts():
    """Import Data Districts to Database"""
    file = os.path.abspath('app/resources') + "/districts.json"
    json_data = open(file).read()
    json_obj = json.loads(json_data)
    for row in json_obj:
        try:
            data = Districts(id=row['id'],
                             name=row['name'],
                             regency_id=row['regency_id'])
            db.session.add(data)
            db.session.commit()
            print('Import data kecamatan {} telah selesai.'.format(data.name))
        except exc.IntegrityError:
            print('Data kecamatan {} sudah ada !!'.format(data.name))
            db.session.rollback()


def import_villages():
    """Import Data Districts to Database"""
    file = os.path.abspath('app/resources') + "/villages.json"
    json_data = open(file).read()
    json_obj = json.loads(json_data)
    for row in json_obj:
        try:
            data = Villages(id=row['id'],
                            name=row['name'],
                            district_id=row['district_id'])
            db.session.add(data)
            db.session.commit()
            print('Import data desa {} telah selesai.'.format(data.name))
        except exc.IntegrityError:
            print('Data desa {} sudah ada !!'.format(data.name))
            db.session.rollback()
