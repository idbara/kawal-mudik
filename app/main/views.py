from flask import Blueprint, render_template
from sqlalchemy import func, and_
from app import cache
from app.models import (DataODP, DataPDP, DataPositif, CovidPerantau,
                        DeteksiSakit, Districts)
from datetime import date

main = Blueprint('main', __name__)


@main.route('/')
@cache.cached(timeout=60)
def index():
    today = date.today()
    odp = DataODP.query.order_by(DataODP.id.desc()).first()
    pdp = DataPDP.query.order_by(DataPDP.id.desc()).first()
    positif = DataPositif.query.order_by(DataPositif.id.desc()).first()
    mudik_today = CovidPerantau.query.filter_by(tgl_mudik=today).count()
    news_pemudik = CovidPerantau.query.order_by(
        CovidPerantau.id.desc()).limit(5).all()
    news_laporsakit = DeteksiSakit.query.order_by(
        DeteksiSakit.id.desc()).limit(5).all()
    daily_pemudik = CovidPerantau.query.with_entities(
        CovidPerantau.tgl_mudik,
        func.count(CovidPerantau.first_name).label('count')).group_by(
            CovidPerantau.tgl_mudik).filter(
                and_(CovidPerantau.tgl_mudik <= today,
                     CovidPerantau.tgl_mudik >= '2020-01-01')).order_by(
                         CovidPerantau.tgl_mudik.asc()).all()
    pemudik_kecamatan = Districts.query.outerjoin(CovidPerantau).with_entities(
        Districts.id, Districts.name,
        func.count(CovidPerantau.first_name).label('total')).group_by(
            Districts.id, Districts.name).all()
    return render_template('homepage/index.html',
                           odp=odp,
                           pdp=pdp,
                           positif=positif,
                           mudik_today=mudik_today,
                           news_pemudik=news_pemudik,
                           news_laporsakit=news_laporsakit,
                           daily_pemudik=daily_pemudik,
                           pemudik_kecamatan=pemudik_kecamatan)


@main.route('/mudik')
def mudik():
    today = date.today()
    odp = DataODP.query.order_by(DataODP.id.desc()).first()
    pdp = DataPDP.query.order_by(DataPDP.id.desc()).first()
    positif = DataPositif.query.order_by(DataPositif.id.desc()).first()
    mudik_today = CovidPerantau.query.filter_by(tgl_mudik=today).count()
    news_pemudik = CovidPerantau.query.order_by(
        CovidPerantau.id.desc()).limit(5).all()
    news_laporsakit = DeteksiSakit.query.order_by(
        DeteksiSakit.id.desc()).limit(5).all()
    daily_pemudik = CovidPerantau.query.with_entities(
        CovidPerantau.tgl_mudik,
        func.count(CovidPerantau.first_name).label('count')).group_by(
            CovidPerantau.tgl_mudik).filter(
                and_(CovidPerantau.tgl_mudik <= today,
                     CovidPerantau.tgl_mudik >= '2020-01-01')).order_by(
                         CovidPerantau.tgl_mudik.asc()).all()
    pemudik_kecamatan = Districts.query.outerjoin(CovidPerantau).with_entities(
        Districts.id, Districts.name,
        func.count(CovidPerantau.first_name).label('total')).group_by(
            Districts.id, Districts.name).all()
    return render_template('homepage/index.html',
                           odp=odp,
                           pdp=pdp,
                           positif=positif,
                           mudik_today=mudik_today,
                           news_pemudik=news_pemudik,
                           news_laporsakit=news_laporsakit,
                           daily_pemudik=daily_pemudik,
                           pemudik_kecamatan=pemudik_kecamatan)
