from .. import db, ma
from marshmallow import Schema, fields
from app.models import (CovidPerantau)


class CovidPerantauJumlahPerDesaSchema(ma.ModelSchema):
    desa_id = fields.Int()
    pemudik = fields.Int(dump_only=True)


class CovidPerantauSchema(ma.ModelSchema):
    class Meta:
        model = CovidPerantau


covid_perantau_jumlah_per_desa_schema = CovidPerantauJumlahPerDesaSchema(
    many=True)
covid_perantau_schema = CovidPerantauSchema(many=True)