from flask import (Blueprint, jsonify, request)
from sqlalchemy import func, and_
from app.schema import *
from app.models import (Provinces, Regencies, Districts, Villages,
                        DataODP, DataPDP, DataPositif)

api = Blueprint('api', __name__)


@api.route('/', methods=['GET'])
def index():
    return jsonify("Api Kawal Mudik")


@api.route('/covid', methods=['GET'])
@api.route('/covid/', methods=['GET'])
def Covid():
    return jsonify("Api Kawal Mudik Covid")


@api.route('/tujuan_kecamatan', methods=['GET'])
def CovidTujuanKecamatan():
    q = [(row.id, row.name) for row in Districts.query.all()]
    return jsonify(q)


@api.route('/tujuan_desa', methods=['GET'])
def CovidTujuanDesa():
    district_id = request.args.get('district_id', type=int)
    q = [(row.id, row.name, row.district_id)
         for row in Villages.query.filter_by(district_id=district_id).all()]
    return jsonify(q)


@api.route('/asal_provinsi', methods=['GET'])
def CovidAsalProvinsi():
    q = [(row.id, row.name) for row in Provinces.query.all()]
    return jsonify(q)


@api.route('/asal_kabupaten', methods=['GET'])
def CovidAsalKabupaten():
    province_id = request.args.get('province_id', type=int)
    q = [(row.id, row.name, row.province_id)
         for row in Regencies.query.filter_by(province_id=province_id).all()]
    return jsonify(q)


@api.route('/covid/kabupaten', methods=['GET'])
@api.route('/covid/kabupaten/', methods=['GET'])
def CovidKabupaten():
    kabupaten = DataODP.query.join(DataPDP, DataODP.id != None).join(
        DataPositif, DataODP.id != None).with_entities(
            DataODP.odp_pemantauan, DataODP.odp_selesai,
            DataPDP.pdp_pengawasan, DataPDP.pdp_selesai,
            DataPDP.pdp_meninggal_negatif, DataPositif.positif_dirawat,
            DataPositif.positif_sembuh,
            DataPositif.positif_meninggal).order_by(
                DataODP.created.desc(), DataPDP.created.desc(),
                DataPositif.created.desc()).first()

    return jsonify({
        'odp_pemantauan': kabupaten[0],
        'odp_selesai': kabupaten[1],
        'pdp_pengawasan': kabupaten[2],
        'pdp_selesai': kabupaten[3],
        'pdp_meninggal_negatif': kabupaten[4],
        'positif_dirawat': kabupaten[5],
        'positif_sembuh': kabupaten[6],
        'positif_meninggal': kabupaten[7]
    })

@api.route('/pemudik', methods=['GET', 'POST'])
@api.route('/pemudik/', methods=['GET', 'POST'])
def pemudik():
    desa = Villages.query.outerjoin(CovidPerantau).with_entities(
        Villages.id.label('desa_id'),
        func.count(CovidPerantau.tujuan_desa).label('pemudik')).group_by(
            Villages.id).all()
    result = covid_perantau_jumlah_per_desa_schema.dump(desa, many=True)
    return jsonify(result)
