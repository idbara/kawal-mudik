from app import db
from flask_wtf import FlaskForm
from wtforms import ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.fields import (PasswordField, StringField, SubmitField,
                            IntegerField)
from wtforms.fields.html5 import EmailField
from wtforms.validators import (Email, EqualTo, InputRequired, Length)
from app.models import Role, User


class ChangeUserEmailForm(FlaskForm):
    email = EmailField('New email',
                       validators=[InputRequired(),
                                   Length(1, 64),
                                   Email()])
    submit = SubmitField('Update email')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')


class ChangeAccountTypeForm(FlaskForm):
    role = QuerySelectField(
        'New account type',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Role).order_by('permissions'))
    submit = SubmitField('Update role')


class InviteUserForm(FlaskForm):
    role = QuerySelectField(
        'Account type',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Role).order_by('permissions'))
    first_name = StringField('First name',
                             validators=[InputRequired(),
                                         Length(1, 64)])
    last_name = StringField('Last name',
                            validators=[InputRequired(),
                                        Length(1, 64)])
    email = EmailField('Email',
                       validators=[InputRequired(),
                                   Length(1, 64),
                                   Email()])
    submit = SubmitField('Invite')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')


class NewUserForm(InviteUserForm):
    password = PasswordField('Password',
                             validators=[
                                 InputRequired(),
                                 EqualTo('password2', 'Passwords must match.')
                             ])
    password2 = PasswordField('Confirm password', validators=[InputRequired()])

    submit = SubmitField('Create')


class ODPForm(FlaskForm):
    odp_pemantauan = IntegerField(
        'ODP Pemantauan',
        validators=[InputRequired('Masukan Total ODP Pemantauan')])
    odp_selesai = IntegerField(
        'ODP Selesai', validators=[InputRequired('Masukan Total ODP Selesai')])
    submit = SubmitField('Edit Data')


class PDPForm(FlaskForm):
    pdp_pengawasan = IntegerField(
        'PDP Dalam Pengawasan',
        validators=[InputRequired('Masukan Total PDP Dalam Pengawasan')])
    pdp_selesai = IntegerField(
        'PDP Selesai',
        validators=[InputRequired('Masukan Total PDP yang Selesai')])
    pdp_meninggal_negatif = IntegerField(
        'PDP Negatif Meninggal',
        validators=[InputRequired('Masukan Total PDP Negatif Meninggal')])
    submit = SubmitField('Edit Data')


class PositifForm(FlaskForm):
    positif_dirawat = IntegerField(
        'Positif Dirawat',
        validators=[InputRequired('Masukan Total Positif Dirawat')])
    positif_sembuh = IntegerField(
        'Positif Sembuh',
        validators=[InputRequired('Masukan Total Positif Sembuh')])
    positif_meninggal = IntegerField(
        'Positif Meninggal',
        validators=[InputRequired('Masukan Total Positif Meninggal')])
    submit = SubmitField('Edit Data')
