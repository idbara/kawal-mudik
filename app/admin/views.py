from flask import (Blueprint, abort, flash, redirect, render_template, url_for)
from flask_login import current_user, login_required
from flask_rq import get_queue

from app import db
from app.admin.forms import (ChangeAccountTypeForm, ChangeUserEmailForm,
                             InviteUserForm, NewUserForm, ODPForm, PDPForm,
                             PositifForm)
from app.decorators import required_roles
from app.email import send_email
from app.models import (Role, User, Provinces, Regencies, Districts, Villages,
                        DataODP, DataPDP, DataPositif)

admin = Blueprint('admin', __name__)


@admin.route('/')
@login_required
@required_roles('Administrator')
def index():
    """Admin dashboard page."""
    return render_template('admin/index.html')


@admin.route('/new-user', methods=['GET', 'POST'])
@login_required
@required_roles('Administrator')
def new_user():
    """Create a new user."""
    form = NewUserForm()
    if form.validate_on_submit():
        user = User(role=form.role.data,
                    first_name=form.first_name.data,
                    last_name=form.last_name.data,
                    email=form.email.data,
                    password=form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('User {} successfully created'.format(user.full_name()),
              'form-success')
    return render_template('admin/new_user.html', form=form)


@admin.route('/invite-user', methods=['GET', 'POST'])
@login_required
@required_roles('Administrator')
def invite_user():
    """Invites a new user to create an account and set their own password."""
    form = InviteUserForm()
    if form.validate_on_submit():
        user = User(role=form.role.data,
                    first_name=form.first_name.data,
                    last_name=form.last_name.data,
                    email=form.email.data)
        db.session.add(user)
        db.session.commit()
        token = user.generate_confirmation_token()
        invite_link = url_for('account.join_from_invite',
                              user_id=user.id,
                              token=token,
                              _external=True)
        get_queue().enqueue(
            send_email,
            recipient=user.email,
            subject='You Are Invited To Join',
            template='account/email/invite',
            user=user,
            invite_link=invite_link,
        )
        flash('User {} successfully invited'.format(user.full_name()),
              'form-success')
    return render_template('admin/new_user.html', form=form)


"""Show All Registered Users is Active"""
@admin.route('/users')
@login_required
@required_roles('Administrator')
def registered_users():
    """View all active registered users"""
    title = "Pengguna Aktif"
    description = "SIDEKEM"
    users = User.query.join(Provinces, User.province_id == Provinces.id).join(
        Regencies, User.regency_id == Regencies.id).join(
            Districts, User.district_id == Districts.id).join(
                Villages, User.village_id == Villages.id).join(
                    Role, User.role_id == Role.id).add_columns(
                        User.id, User.first_name, User.last_name, User.email,
                        Provinces.name.label('province_name'),
                        Regencies.name.label('regency_name'),
                        Districts.name.label('district_name'),
                        Villages.name.label('village_name'),
                        Role.name.label('role_name'),
                        User.file_sk).filter(User.confirmed == "True").all()
    return render_template('admin/registered_users.html',
                           users=users,
                           title=title,
                           description=description)


"""Show All Registration Users for Activation"""
@admin.route('/activation')
@login_required
@required_roles('Administrator')
def activation_users():
    """View all registration users for activate"""
    title = "Aktifasi Pengguna"
    description = "SIDEKEM"
    users = User.query.join(Provinces, User.province_id == Provinces.id).join(
        Regencies, User.regency_id == Regencies.id).join(
            Districts, User.district_id == Districts.id).join(
                Villages, User.village_id == Villages.id).join(
                    Role, User.role_id == Role.id).add_columns(
                        User.id, User.first_name, User.last_name, User.email,
                        Provinces.name.label('province_name'),
                        Regencies.name.label('regency_name'),
                        Districts.name.label('district_name'),
                        Villages.name.label('village_name'),
                        Role.name.label('role_name'),
                        User.file_sk).filter(User.confirmed == "False").all()
    return render_template('admin/activation_users.html',
                           users=users,
                           title=title,
                           description=description)


"""Oprek"""
@admin.route('/activated-account/<int:user_id>')
@login_required
def activated_request(user_id):
    """get email user"""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        flash('Akun tidak ditemukan', 'error')
    else:
        user.activation_account()
        print(user)
        """Respond to new user's request to confirm their account."""
        confirm_link = url_for('account.login', _external=True)
        get_queue().enqueue(
            send_email,
            recipient=user.email,
            subject='Aktivasi Akun',
            template='account/email/activated',
            # current_user is a LocalProxy, we want the underlying user object
            user=User.query.filter_by(id=user_id).first(),
            confirm_link=confirm_link)
        flash(
            'Pemberitahuan Aktifasi berhasil di kirim ke email {}.'.format(
                user.email), 'info')
    return redirect(url_for('admin.activation_users'))


@admin.route('/user/<int:user_id>')
@admin.route('/user/<int:user_id>/info')
@login_required
@required_roles('Administrator')
def user_info(user_id):
    """View a user's profile."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    return render_template('admin/manage_user.html', user=user)


@admin.route('/user/<int:user_id>/change-email', methods=['GET', 'POST'])
@login_required
@required_roles('Administrator')
def change_user_email(user_id):
    """Change a user's email."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    form = ChangeUserEmailForm()
    if form.validate_on_submit():
        user.email = form.email.data
        db.session.add(user)
        db.session.commit()
        flash(
            'Email for user {} successfully changed to {}.'.format(
                user.full_name(), user.email), 'form-success')
    return render_template('admin/manage_user.html', user=user, form=form)


@admin.route('/user/<int:user_id>/change-account-type',
             methods=['GET', 'POST'])
@login_required
@required_roles('Administrator')
def change_account_type(user_id):
    """Change a user's account type."""
    if current_user.id == user_id:
        flash(
            'You cannot change the type of your own account. Please ask '
            'another administrator to do this.', 'error')
        return redirect(url_for('admin.user_info', user_id=user_id))

    user = User.query.get(user_id)
    if user is None:
        abort(404)
    form = ChangeAccountTypeForm()
    if form.validate_on_submit():
        user.role = form.role.data
        db.session.add(user)
        db.session.commit()
        flash(
            'Role for user {} successfully changed to {}.'.format(
                user.full_name(), user.role.name), 'form-success')
    return render_template('admin/manage_user.html', user=user, form=form)


@admin.route('/user/<int:user_id>/delete')
@login_required
@required_roles('Administrator')
def delete_user_request(user_id):
    """Request deletion of a user's account."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    return render_template('admin/manage_user.html', user=user)


@admin.route('/user/<int:user_id>/_delete')
@login_required
@required_roles('Administrator')
def delete_user(user_id):
    """Delete a user's account."""
    if current_user.id == user_id:
        flash(
            'You cannot delete your own account. Please ask another '
            'administrator to do this.', 'error')
    else:
        user = User.query.filter_by(id=user_id).first()
        db.session.delete(user)
        db.session.commit()
        flash('Successfully deleted user %s.' % user.full_name(), 'success')
    return redirect(url_for('admin.registered_users'))


"""View All Data ODP"""
@admin.route('/datapokok/odp', methods=['GET', 'POST'])
@login_required
@required_roles('Administrator')
def DataOdp():
    title = "Data ODP"
    odp = DataODP.query.order_by(DataODP.id.asc()).all()
    return render_template('admin/odp.html', odp=odp, title=title)


"""Edit Data ODP"""
@admin.route('/datapokok/odp/<int:id>', methods=['GET', 'POST'])
@login_required
@required_roles('Administrator')
def DataOdpEdit(id):
    title = "Data ODP"
    page = "Edit Data"
    odp = DataODP.query.order_by(DataODP.id.asc()).all()
    data = DataODP.query.filter_by(id=id).first_or_404()
    form = ODPForm(obj=data)
    if form.validate_on_submit():
        form.populate_obj(data)
        db.session.commit()
        flash('Data ODP berhasil diedit', 'success')
        return redirect(url_for('admin.DataOdp'))
    return render_template('admin/odp.html',
                           odp=odp,
                           data=data,
                           form=form,
                           title=title,
                           page=page)


"""View All Data PDP"""
@admin.route('/datapokok/pdp', methods=['GET', 'POST'])
@login_required
@required_roles('Administrator')
def DataPdp():
    title = "Data PDP"
    pdp = DataPDP.query.order_by(DataPDP.id.asc()).all()
    return render_template('admin/pdp.html', pdp=pdp, title=title)


"""Edit Data PDP"""
@admin.route('/datapokok/pdp/<int:id>', methods=['GET', 'POST'])
@login_required
@required_roles('Administrator')
def DataPdpEdit(id):
    title = "Data PDP"
    page = "Edit Data"
    pdp = DataPDP.query.order_by(DataPDP.id.asc()).all()
    data = DataPDP.query.filter_by(id=id).first_or_404()
    form = PDPForm(obj=data)
    if form.validate_on_submit():
        form.populate_obj(data)
        db.session.commit()
        flash('Data PDP berhasil diedit', 'success')
        return redirect(url_for('admin.DataPdp'))
    return render_template('admin/pdp.html',
                           pdp=pdp,
                           data=data,
                           form=form,
                           title=title,
                           page=page)


"""View All Data Positif"""
@admin.route('/datapokok/positif', methods=['GET', 'POST'])
@login_required
@required_roles('Administrator')
def DataPositifCorona():
    title = "Data PDP"
    positif = DataPositif.query.order_by(DataPositif.id.asc()).all()
    return render_template('admin/positif.html', positif=positif, title=title)


"""Edit Data PDP"""
@admin.route('/datapokok/positif/<int:id>', methods=['GET', 'POST'])
@login_required
@required_roles('Administrator')
def DataPositifCoronaEdit(id):
    title = "Data Positif"
    page = "Edit Data"
    positif = DataPositif.query.order_by(DataPositif.id.asc()).all()
    data = DataPositif.query.filter_by(id=id).first_or_404()
    form = PositifForm(obj=data)
    if form.validate_on_submit():
        form.populate_obj(data)
        db.session.commit()
        flash('Data PDP berhasil diedit', 'success')
        return redirect(url_for('admin.DataPositifCorona'))
    return render_template('admin/positif.html',
                           positif=positif,
                           data=data,
                           form=form,
                           title=title,
                           page=page)


"""Test Email"""
@admin.route('/test/email/<string:email>', methods=['GET', 'POST'])
@login_required
@required_roles('Administrator')
def testemail(email):
    """Respond to new user's request to confirm their account."""
    confirm_link = url_for('account.login', _external=True)
    get_queue().enqueue(
        send_email,
        recipient=email,
        subject='Aktivasi Akun',
        template='account/email/activated',
        # current_user is a LocalProxy, we want the underlying user object
        user=User.query.first(),
        confirm_link=confirm_link)
    return "Kirim email ke " + email
