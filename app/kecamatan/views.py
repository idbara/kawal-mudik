from flask import Blueprint, render_template
from flask_login import login_required
from app.decorators import required_roles
from flask_login import (current_user)
from datetime import date

from app.models import CovidPerantau, DeteksiSakit

kecamatan = Blueprint('kecamatan', __name__)


@kecamatan.route('/')
@login_required
@required_roles('Kecamatan')
def index():
    return render_template('kecamatan/index.html')


"""Covid19"""
@kecamatan.route('/perantau/view/data')
@login_required
@required_roles('Kecamatan')
def PerantauDataView():
    """View all Data Perantau mau mudik"""
    title = "Data Perantau"
    page = "View Data Perantau"
    description = "SIDEKEM"
    data = CovidPerantau.query.order_by(CovidPerantau.id.desc()).filter_by(
        tujuan_kecamatan=current_user.district_id).all()
    print(data)
    return render_template('covid19/data/index.html',
                           data=data,
                           title=title,
                           page=page,
                           description=description)


@kecamatan.route('/perantau/print/data')
@login_required
@required_roles('Kecamatan')
def PerantauDataPrint():
    """View all Data Perantau mau mudik"""
    title = "Data Perantau"
    page = "Print Data"
    description = "SIDEKEM"
    data = CovidPerantau.query.order_by(CovidPerantau.id.desc()).filter_by(
        tujuan_kecamatan=current_user.district_id).all()
    print(data)
    return render_template('covid19/data/index.html',
                           data=data,
                           title=title,
                           page=page,
                           description=description)


@kecamatan.route('/perantau/arrival-today/data')
@login_required
@required_roles('Kecamatan')
def PerantauDataArrivalToday():
    """View all Data Perantau mau mudik"""
    today = date.today()
    title = "Data Perantau"
    page = "Kedatangan Hari Ini"
    description = "SIDEKEM"
    data = CovidPerantau.query.filter_by(
        tujuan_kecamatan=current_user.district_id, tgl_mudik=today).all()
    print(data)
    return render_template('covid19/data/index.html',
                           data=data,
                           title=title,
                           page=page,
                           description=description)


"""Covid19"""
@kecamatan.route('/deteksi-sakit/view/data')
@login_required
@required_roles('Kecamatan')
def DeteksiSakitDataView():
    """View all Data Deteksi Sakit"""
    title = "Data Deteksi Sakit"
    page = "View Data Deteksi Sakit"
    description = "Desa vs Corona"
    data = DeteksiSakit.query.order_by(DeteksiSakit.id.desc()).filter_by(
        kecamatan=current_user.district_id).all()
    return render_template('covid19/data/deteksi_sakit.html',
                           data=data,
                           title=title,
                           page=page,
                           description=description)


@kecamatan.route('/deteksi-sakit/print/data')
@login_required
@required_roles('Kecamatan')
def DeteksiSakitDataPrint():
    """Print all Data Deteksi Sakit"""
    title = "Data Deteksi Sakit"
    page = "Print Data Deteksi Sakit"
    description = "Desa vs Corona"
    data = DeteksiSakit.query.order_by(DeteksiSakit.id.desc()).filter_by(
        kecamatan=current_user.district_id).all()
    return render_template('covid19/data/deteksi_sakit.html',
                           data=data,
                           title=title,
                           page=page,
                           description=description)
