from app.models import (DataODP, DataPDP, DataPositif)
from app import db
from app.scrape.covid import scrape_code
import requests
from bs4 import BeautifulSoup


def scrape():
    scrape_code()
    covid = []
    URL = 'https://infocorona.pemalangkab.go.id/'
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, 'html.parser')

    result = soup.find_all('div', class_='card card-nav-tabs')
    for result in result:
        d = {}
        title_elem = result.find('h4', class_='card-header')
        d['status'] = title_elem.text.strip()
        body_elem = result.find_all('div', class_='card-body')
        for body_elem in body_elem:
            no = 1
            total = body_elem.find('h2', class_='card-title text-center')
            d['total'] = total.text.strip()
            row = body_elem.find_all('h3', class_='card-title text-center')
            for row in row:
                d['det' + str(no)] = row.text.strip()
                no = no + 1
        covid.append(d)

    q = DataODP(odp_pemantauan=covid[0]['det1'], odp_selesai=covid[0]['det2'])
    db.session.add(q)
    db.session.commit()

    q = DataPDP(pdp_pengawasan=covid[1]['det1'],
                pdp_selesai=covid[1]['det2'],
                pdp_meninggal_negatif=covid[1]['det3'])
    db.session.add(q)
    db.session.commit()

    q = DataPositif(positif_dirawat=covid[2]['det1'],
                    positif_sembuh=covid[2]['det2'],
                    positif_meninggal=covid[2]['det3'])
    db.session.add(q)
    db.session.commit()
    return "done"
