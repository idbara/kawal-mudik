import requests
from bs4 import BeautifulSoup


def scrape_code():
    covid = []
    URL = 'https://infocorona.pemalangkab.go.id/'
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, 'html.parser')

    result = soup.find_all('div', class_='card card-nav-tabs')
    for result in result:
        d = {}
        title_elem = result.find('h4', class_='card-header')
        d['status'] = title_elem.text.strip()
        # print(title_elem.text)
        body_elem = result.find_all('div', class_='card-body')
        for body_elem in body_elem:
            no = 1

            total = body_elem.find('h2', class_='card-title text-center')
            d['total'] = total.text.strip()
            # print("total "+total.text)
            row = body_elem.find_all('h3', class_='card-title text-center')
            for row in row:
                # print(row.text)
                d['det' + str(no)] = row.text.strip()
                no = no + 1
        covid.append(d)

    return covid


if __name__ == "__main__":
    print(scrape_code())
