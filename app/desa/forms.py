from flask_wtf import FlaskForm
from wtforms.fields import TextAreaField, SubmitField
from flask_wtf.file import FileField, FileAllowed, FileRequired


class EmbedCodeKab(FlaskForm):
    code = TextAreaField()
    submit = SubmitField('Generate Code')


class EmbedCodePemudik(FlaskForm):
    code = TextAreaField()
    submit = SubmitField('Generate Code')


class EmbedCodeOrangSakit(FlaskForm):
    code = TextAreaField()
    submit = SubmitField('Generate Code')


class EmbedCodeSearchPemudik(FlaskForm):
    code = TextAreaField()
    submit = SubmitField('Generate Code')


class ImportPemudikForm(FlaskForm):
    csv = FileField('Masukan CSV File',
                    validators=[
                        FileRequired('Masukan File Pemudik !!'),
                        FileAllowed(['csv'],
                                    'File yang di upload harus CSV !!')
                    ])
    submit = SubmitField('Import Data')
