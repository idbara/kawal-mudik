from flask import (Blueprint, render_template, url_for, flash, redirect)
from flask_login import login_required
from app.decorators import required_roles
from flask_login import (current_user)
from flask_rq import get_queue

from datetime import datetime
from io import TextIOWrapper
import csv
from sqlalchemy import exc

from app.models import (CovidPerantau, DeteksiSakit, DataODP, DataPDP,
                        DataPositif, Country, Provinces, Regencies)
from app.desa.forms import (EmbedCodeKab, EmbedCodePemudik,
                            EmbedCodeOrangSakit, EmbedCodeSearchPemudik,
                            ImportPemudikForm)
from app.covid19.forms import CovidPerantauForm, CovidPerantauLuarNegeriForm

from app.email import send_email
from app import db

desa = Blueprint('desa', __name__)


@desa.route('/')
@login_required
@required_roles('Desa')
def index():
    today = datetime.today()
    odp = DataODP.query.order_by(DataODP.id.desc()).first()
    pdp = DataPDP.query.order_by(DataPDP.id.desc()).first()
    positif = DataPositif.query.order_by(DataPositif.id.desc()).first()
    mudik_today = CovidPerantau.query.filter_by(
        tgl_mudik=today, tujuan_desa=current_user.village_id).count()
    news_pemudik = CovidPerantau.query.filter_by(
        tujuan_desa=current_user.village_id).order_by(
            CovidPerantau.id.desc()).limit(5).all()
    news_laporsakit = DeteksiSakit.query.filter_by(
        desa=current_user.village_id).order_by(
            DeteksiSakit.id.desc()).limit(5).all()
    total_perantau = CovidPerantau.query.filter_by(
        tujuan_desa=current_user.village_id).count()
    total_perantau_pulang = CovidPerantau.query.filter_by(
        tujuan_desa=current_user.village_id, confirmed=True).count()
    total_sakit = DeteksiSakit.query.filter_by(
        desa=current_user.village_id).count()
    total_sakit_sudah_ditangani = DeteksiSakit.query.filter_by(
        desa=current_user.village_id, confirmed=True).count()
    return render_template(
        'covid19/desa/index.html',
        odp=odp,
        pdp=pdp,
        positif=positif,
        mudik_today=mudik_today,
        news_pemudik=news_pemudik,
        news_laporsakit=news_laporsakit,
        total_perantau=total_perantau,
        total_perantau_pulang=total_perantau_pulang,
        total_sakit=total_sakit,
        total_sakit_sudah_ditangani=total_sakit_sudah_ditangani)


"""Covid19"""
@desa.route('/perantau/view/data')
@login_required
@required_roles('Desa')
def PerantauDataView():
    """View all Data Perantau mau mudik"""
    title = "Data Perantau"
    page = "View Data Perantau"
    description = "Desa vs Corona"
    data = CovidPerantau.query.order_by(CovidPerantau.id.desc()).filter_by(
        tujuan_desa=current_user.village_id).all()
    return render_template('covid19/data/index.html',
                           data=data,
                           title=title,
                           page=page,
                           description=description)


@desa.route('/perantau/tandai-pulang/<int:id>')
@login_required
def TandaiPulang(id):
    """get email perantau"""
    perantau = CovidPerantau.query.filter_by(
        id=id, tujuan_desa=current_user.village_id).first()
    if perantau is None:
        flash('Data perantau tidak ditemukan', 'error')
    else:
        perantau.pulang()
        """Respond to new user's request to confirm their account."""
        get_queue().enqueue(
            send_email,
            recipient=perantau.email,
            subject='Pemberitahuan Kepulangan',
            template='account/email/perantau_pulang',
            # current_user is a LocalProxy, we want the underlying user object
            perantau=CovidPerantau.query.filter_by(
                id=id, tujuan_desa=current_user.village_id).first())
        flash(
            'Pemberitahuan kepulangan di kirim ke email {}.'.format(
                perantau.email), 'info')
    return redirect(url_for('desa.PerantauDataView'))


"""Edit Data Perantau"""
@desa.route('/perantau/edit/<int:id>', methods=['GET', 'POST'])
@login_required
@required_roles('Desa')
def EditPerantau(id):
    title = "Edit Data Pemudik"
    page = "Edit Data"
    perantau = CovidPerantau.query.filter_by(
        id=id, tujuan_desa=current_user.village_id).first_or_404()
    if perantau.asal_negara is None or perantau.asal_negara == 'ID':
        form = CovidPerantauForm(obj=perantau)
        print(form.demam.data)
        if form.validate_on_submit():
            form.populate_obj(perantau)
            db.session.commit()
            flash('Data perantau berhasil diedit', 'success')
            return redirect(url_for('desa.PerantauDataView'))
    else:
        form = CovidPerantauLuarNegeriForm(obj=perantau)
        if form.validate_on_submit():
            form.populate_obj(perantau)
            db.session.commit()
            flash('Data perantau berhasil diedit', 'success')
            return redirect(url_for('desa.PerantauDataView'))
    return render_template('covid19/desa/perantau_edit.html',
                           perantau=perantau,
                           form=form,
                           title=title,
                           page=page)


"""Delete Data Perantau"""
@desa.route('/perantau/delete/<int:id>', methods=['GET', 'POST'])
@login_required
@required_roles('Desa')
def DeletePerantau(id):
    perantau = CovidPerantau.query.filter_by(
        id=id, tujuan_desa=current_user.village_id).first_or_404()
    db.session.delete(perantau)
    db.session.commit()
    flash('Data perantau {} berhasil dihapus'.format(perantau.full_name()),
          'error')
    return redirect(url_for('desa.PerantauDataView'))


@desa.route('/perantau/print/data')
@login_required
@required_roles('Desa')
def PerantauDataPrint():
    """View all Data Perantau mau mudik"""
    title = "Data Perantau"
    page = "Print Data"
    description = "Desa vs Corona"
    data = CovidPerantau.query.order_by(CovidPerantau.id.desc()).filter_by(
        tujuan_desa=current_user.village_id).all()
    return render_template('covid19/data/index.html',
                           data=data,
                           title=title,
                           page=page,
                           description=description)


@desa.route('/perantau/arrival-today/data')
@login_required
@required_roles('Desa')
def PerantauDataArrivalToday():
    """View all Data Perantau mau mudik"""
    today = datetime.today()
    title = "Data Perantau"
    page = "Kedatangan Hari Ini"
    description = "Desa vs Corona"
    data = CovidPerantau.query.filter_by(tujuan_desa=current_user.village_id,
                                         tgl_mudik=today).all()
    return render_template('covid19/data/index.html',
                           data=data,
                           title=title,
                           page=page,
                           description=description)


"""Covid19"""
@desa.route('/deteksi-sakit/view/data')
@login_required
@required_roles('Desa')
def DeteksiSakitDataView():
    """View all Data Deteksi Sakit"""
    title = "Data Deteksi Sakit"
    page = "View Data Deteksi Sakit"
    description = "Desa vs Corona"
    data = DeteksiSakit.query.order_by(
        DeteksiSakit.id.desc()).filter_by(desa=current_user.village_id).all()
    return render_template('covid19/data/deteksi_sakit.html',
                           data=data,
                           title=title,
                           page=page,
                           description=description)


@desa.route('/deteksi-sakit/ditangani/<int:id>')
@login_required
def TandaiSudahDitangani(id):
    """get email perantau"""
    sakit = DeteksiSakit.query.filter_by(id=id,
                                         desa=current_user.village_id).first()
    if sakit is None:
        flash('Data pelapot tidak ditemukan', 'error')
    else:
        sakit.sudah_ditangani()
        """Respond request to ditangani"""
        flash(
            'Laporan atas nama {} sudah ditangani !!!'.format(
                sakit.full_name()), 'success')
    return redirect(url_for('desa.DeteksiSakitDataView'))


@desa.route('/deteksi-sakit/print/data')
@login_required
@required_roles('Desa')
def DeteksiSakitDataPrint():
    """Print all Data Deteksi Sakit"""
    title = "Data Deteksi Sakit"
    page = "Print Data Deteksi Sakit"
    description = "Desa vs Corona"
    data = DeteksiSakit.query.order_by(
        DeteksiSakit.id.desc()).filter_by(desa=current_user.village_id).all()
    return render_template('covid19/data/deteksi_sakit.html',
                           data=data,
                           title=title,
                           page=page,
                           description=description)


@desa.route('/embed-code')
@login_required
@required_roles('Desa')
def EmbedCode():
    title = "Embed Statistik"
    description = "Kawal Mudik"
    form_kab = EmbedCodeKab()
    form_pemudik = EmbedCodePemudik()
    form_sakit = EmbedCodeOrangSakit()
    form_cari_pemudik = EmbedCodeSearchPemudik()
    return render_template('covid19/data/widget.html',
                           title=title,
                           description=description,
                           form_kab=form_kab,
                           form_pemudik=form_pemudik,
                           form_cari_pemudik=form_cari_pemudik,
                           form_sakit=form_sakit)


"""Generate Kode Statistik Covid-19 Kabupaten"""
@desa.route('/embed-code/kabupaten', methods=['GET', 'POST'])
@required_roles('Desa')
def EmbedCodeKabupaten():
    title = "Embed Statistik Covid19 Kabupaten"
    description = "Kawal Mudik"
    link = url_for('covid19.StatistikCoronaKabupaten',
                   _external=True,
                   _scheme='https')
    form_kab = EmbedCodeKab()
    form_kab.code.data = '<iframe width="1300" height="1780" \
        src="{}" \
        frameborder="0" gesture="media" allow="encrypted-media" \
            allowfullscreen></iframe>'.format(link)
    form_pemudik = EmbedCodePemudik()
    form_sakit = EmbedCodeOrangSakit()
    form_cari_pemudik = EmbedCodeSearchPemudik()
    return render_template('covid19/data/widget.html',
                           form_kab=form_kab,
                           form_pemudik=form_pemudik,
                           form_sakit=form_sakit,
                           form_cari_pemudik=form_cari_pemudik,
                           title=title,
                           description=description)


"""Generate Kode Statistik Pemudik Dari Desa"""
@desa.route('/embed-code/pemudik/<int:village_id>', methods=['GET', 'POST'])
@required_roles('Desa')
def EmbedCodePemudikDesa(village_id):
    title = "Embed Statistik Pemudik"
    description = "Kawal Mudik"
    link = url_for('covid19.StatistikPemudikDesa',
                   village_id=current_user.village_id,
                   _external=True,
                   _scheme='https')
    form_kab = EmbedCodeKab()
    form_pemudik = EmbedCodePemudik()
    form_pemudik.code.data = '<iframe width="1300" height="1500" \
        src="{}" frameborder="0" gesture="media" \
            allow="encrypted-media" allowfullscreen></iframe>'.format(link)
    form_sakit = EmbedCodeOrangSakit()
    form_cari_pemudik = EmbedCodeSearchPemudik()
    return render_template('covid19/data/widget.html',
                           form_kab=form_kab,
                           form_pemudik=form_pemudik,
                           form_sakit=form_sakit,
                           form_cari_pemudik=form_cari_pemudik,
                           title=title,
                           description=description)


"""Generate Kode Statistik Orang Sakit Dari Desa"""
@desa.route('/embed-code/lapor-sakit/<int:village_id>',
            methods=['GET', 'POST'])
@required_roles('Desa')
def EmbedCodeSakitDesa(village_id):
    title = "Embed Statistik Lapor Sakit"
    description = "Kawal Mudik"
    link = url_for('covid19.StatistikSakitDesa',
                   village_id=current_user.village_id,
                   _external=True,
                   _scheme='https')
    form_kab = EmbedCodeKab()
    form_pemudik = EmbedCodePemudik()
    form_sakit = EmbedCodeOrangSakit()
    form_sakit.code.data = '<iframe width="1300" height="1500" \
        src="{}" frameborder="0" gesture="media" \
            allow="encrypted-media" allowfullscreen></iframe>'.format(link)
    form_cari_pemudik = EmbedCodeSearchPemudik()
    return render_template('covid19/data/widget.html',
                           form_kab=form_kab,
                           form_pemudik=form_pemudik,
                           form_sakit=form_sakit,
                           form_cari_pemudik=form_cari_pemudik,
                           title=title,
                           description=description)


"""Generate Kode Statistik Orang Sakit Dari Desa"""
@desa.route('/embed-code/cari-pemudik', methods=['GET', 'POST'])
@required_roles('Desa')
def EmbedCodeSearchDataPemudik():
    title = "Embed Pencarian Data Pemudik"
    description = "Kawal Mudik"
    link = url_for('covid19.SearchPemudik', _external=True, _scheme='https')
    form_kab = EmbedCodeKab()
    form_pemudik = EmbedCodePemudik()
    form_sakit = EmbedCodeOrangSakit()
    form_cari_pemudik = EmbedCodeSearchPemudik()
    form_cari_pemudik.code.data = '<iframe width="1300" height="700" \
        src="{}" frameborder="0" gesture="media" \
            allow="encrypted-media" allowfullscreen></iframe>'.format(link)
    return render_template('covid19/data/widget.html',
                           form_kab=form_kab,
                           form_pemudik=form_pemudik,
                           form_sakit=form_sakit,
                           form_cari_pemudik=form_cari_pemudik,
                           title=title,
                           description=description)


"""Import Data Pemudik"""
@desa.route('/import-pemudik', methods=['GET', 'POST'])
@login_required
@required_roles('Desa')
def ImportPemudik():
    title = "Import Data Pemudik"
    description = "Kawal Mudik"
    data = CovidPerantau.query.order_by(CovidPerantau.id.desc()).filter_by(
        tujuan_desa=current_user.village_id).all()
    country = Country.query.all()
    province = Provinces.query.all()
    regency = Regencies.query.all()
    form = ImportPemudikForm()
    if form.validate_on_submit():
        csv_file = form.csv.data
        csv_file = TextIOWrapper(csv_file, encoding='utf-8')
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader, None)
        for row in csv_reader:
            try:
                first_name = (row[0])
                if first_name == '' or None:
                    flash(('Nama depan tidak boleh kosong !!'), 'error')
                    return redirect(url_for('desa.ImportPemudik'))
                nik = row[2]
                if nik == '' or None:
                    flash(('NIK tidak boleh kosong !!'), 'error')
                    return redirect(url_for('desa.ImportPemudik'))
                tgl_lahir = row[3]
                try:
                    tgl_lahir is not datetime.strptime(tgl_lahir, '%Y-%m-%d')
                except Exception:
                    flash(('Format Tanggal Salah Pastikan YYYY-MM-DD'),
                          'error')
                    return redirect(url_for('desa.ImportPemudik'))
                if tgl_lahir == '' or None:
                    flash(('Tanggal Lahir tidak boleh kosong !!'), 'error')
                    return redirect(url_for('desa.ImportPemudik'))
                jk = row[4]
                if jk == '':
                    flash(('Data jenis kelamin tidak boleh kosong !!'),
                          'error')
                    return redirect(url_for('desa.ImportPemudik'))
                alamat = row[7]
                if alamat == '':
                    flash(('Alamat tidak boleh kosong'), 'error')
                    return redirect(url_for('desa.ImportPemudik'))
                for negara in country:
                    if row[10] == negara.name:
                        country_id = negara.id
                for provinsi in province:
                    if row[11] == provinsi.name:
                        province_id = provinsi.id
                for kabupaten in regency:
                    if row[12] == kabupaten.name:
                        regency_id = kabupaten.id
                        demam = True if row[16] == "ya" else False
                        batuk_pilek = True if row[17] == "ya" else False
                        sakit_tenggorokan = True \
                            if row[18] == "ya" else False
                        sesak_napas = True if row[19] == "ya" else False
                        status = True if row[20] == "pulang" else False
                        q = CovidPerantau(
                            first_name=first_name,
                            last_name=row[1],
                            nik=nik,
                            tgl_lahir=tgl_lahir,
                            jk=jk,
                            shdk=row[5],
                            jml_ak_dan_pemudik=row[6],
                            alamat=row[7],
                            email=row[8],
                            telp=row[9],
                            tujuan_kecamatan=current_user.district_id,
                            tujuan_desa=current_user.village_id,
                            asal_negara=country_id,
                            asal_provinsi=province_id,
                            asal_kabupaten=regency_id,
                            tgl_mudik=row[13],
                            alasan_mudik=row[14],
                            alasan_mudik_lainnya=row[15],
                            demam=demam,
                            batuk_pilek=batuk_pilek,
                            sakit_tenggorokan=sakit_tenggorokan,
                            sesak_napas=sesak_napas,
                            confirmed=status)
                        db.session.add(q)
                        db.session.commit()
            # Skip Data Already Exist and Count
            except exc.IntegrityError:
                db.session.rollback()
                flash(('Periksa kembali data anda !!'.format(row[1])), 'error')
                return redirect(url_for('desa.ImportPemudik'))
        flash(('Import Data Sukses'), 'success')
        data = CovidPerantau.query.order_by(CovidPerantau.id.desc()).filter_by(
            tujuan_desa=current_user.village_id).all()
    return render_template('covid19/desa/import_pemudik.html',
                           title=title,
                           data=data,
                           country=country,
                           province=province,
                           regency=regency,
                           description=description,
                           form=form)


@desa.route('/malicious')
@login_required
@required_roles('Desa')
def Malicious():
    return render_template('covid19/desa/malicious.html', id=id)


"""Delete Data Perantau"""
@desa.route('/malicious/delete/all', methods=['GET', 'POST'])
@login_required
@required_roles('Desa')
def DeleteAllPerantau():
    CovidPerantau.query.filter_by(
        tujuan_desa=current_user.village_id).delete()
    flash('Data perantau berhasil dihapus', 'success')
    return redirect(url_for('desa.Malicious'))
