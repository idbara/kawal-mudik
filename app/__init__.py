import os

from flask import Flask
from flask_compress import Compress
from flask_login import LoginManager
from flask_mail import Mail
from flask_rq import RQ
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CSRFProtect
from flask_bootstrap import Bootstrap
from flask_babel import Babel
from flask_images import Images
from flask_moment import Moment
from flask_marshmallow import Marshmallow
from flask_minify import minify
from flask_caching import Cache
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from elasticsearch import Elasticsearch
from config import config as Config

basedir = os.path.abspath(os.path.dirname(__file__))

COMPRESS_MIMETYPES = [
    'text/html', 'text/css', 'text/xml', 'application/json',
    'application/javascript'
]
COMPRESS_LEVEL = 6
COMPRESS_MIN_SIZE = 500

mail = Mail()
db = SQLAlchemy()
ma = Marshmallow()
csrf = CSRFProtect()
compress = Compress()
bootstrap = Bootstrap()
babel = Babel()
moment = Moment()
images = Images()
minify = minify(html=True, js=True, cssless=True)
cache = Cache()

# Set up Flask-Login
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'account.login'


def create_app(config):
    app = Flask(__name__)
    config_name = config

    if not isinstance(config, str):
        config_name = os.getenv('FLASK_CONFIG', 'default')

    app.config.from_object(Config[config_name])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    # not using sqlalchemy event system, hence disabling it

    sentry_sdk.init(dsn=app.config['SENTRY_DSN'],
                    integrations=[FlaskIntegration()])

    Config[config_name].init_app(app)

    # Set up extensions
    mail.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    csrf.init_app(app)
    compress.init_app(app)
    bootstrap.init_app(app)
    babel.init_app(app)
    moment.init_app(app)
    images.init_app(app)
    minify.init_app(app)
    cache.init_app(app)
    app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']]) \
        if app.config['ELASTICSEARCH_URL'] else None

    RQ(app)

    # Register Jinja template functions
    from .utils import register_template_utils
    register_template_utils(app)

    # Configure SSL if platform supports it
    if not app.debug and not app.testing and not app.config['SSL_DISABLE']:
        from flask_sslify import SSLify
        SSLify(app)

    # Create app blueprints
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .kecamatan import kecamatan as kecamatan_blueprint
    app.register_blueprint(kecamatan_blueprint, url_prefix='/kecamatan')

    from .desa import desa as desa_blueprint
    app.register_blueprint(desa_blueprint, url_prefix='/desa')

    from .dinkes import dinkes as dinkes_blueprint
    app.register_blueprint(dinkes_blueprint, url_prefix='/dinkes')

    from .account import account as account_blueprint
    app.register_blueprint(account_blueprint, url_prefix='/account')

    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from .api import api as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='/api')

    from .covid19 import covid19 as covid19_blueprint
    app.register_blueprint(covid19_blueprint, url_prefix='/covid19')

    from .maps import maps as maps_blueprint
    app.register_blueprint(maps_blueprint, url_prefix='/maps')

    return app
