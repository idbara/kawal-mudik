from flask import (Blueprint, render_template, flash, redirect, url_for,
                   jsonify)
from sqlalchemy import func, and_
from flask_rq import get_queue

from app.models import (CovidPerantau, CovidDeteksiMandiri, DeteksiSakit,
                        Posko, RumahSakit, Puskesmas, DataODP, DataPDP,
                        DataPositif, Villages)
from app.schema import (covid_perantau_jumlah_per_desa_schema,
                        covid_perantau_schema)
from app import db
from app.email import send_email
from config import Config
from datetime import date

maps = Blueprint('maps', __name__)


@maps.route('/', methods=['GET', 'POST'])
def index():
    title = "MAPS"
    description = "Kawal Mudik"
    return render_template('maps/index.html',
                           title=title,
                           description=description)


@maps.route('/pemudik_per_desa', methods=['GET', 'POST'])
def pemudik_per_desa():
    desa = Villages.query.outerjoin(CovidPerantau).with_entities(
        Villages.id.label('desa_id'),
        func.count(CovidPerantau.tujuan_desa).label('pemudik')).group_by(
            Villages.id).all()
    result = covid_perantau_jumlah_per_desa_schema.dump(desa, many=True)
    return jsonify(result)


# @maps.route('/pemudik', methods=['GET', 'POST'])
# def pemudik():
#     desa = CovidPerantau.query.all()
#     result = covid_perantau_schema.dump(desa).data
#     return jsonify(result)