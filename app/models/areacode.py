from .. import db


class Country(db.Model):
    __tablename__ = "country"
    id = db.Column(db.String(100), nullable=False, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    perantau = db.relationship('CovidPerantau',
                               backref='perantau_luarnegeri',
                               lazy=True)

    def __repr__(self):
        return '<Countries {}>'.format(self.name)


class Provinces(db.Model):
    __tablename__ = "provinces"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    provinces = db.relationship('Regencies', backref='provinces', lazy=True)
    perantau = db.relationship('CovidPerantau',
                               backref='perantau_provinsi',
                               lazy=True)

    def __repr__(self):
        return '<Province {}>'.format(self.name)


class Regencies(db.Model):
    __tablename__ = "regencies"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    province_id = db.Column(db.Integer,
                            db.ForeignKey('provinces.id',
                                          ondelete="CASCADE",
                                          onupdate="CASCADE"),
                            nullable=False)
    regencies = db.relationship('Districts', backref='regencies', lazy=True)
    perantau = db.relationship('CovidPerantau',
                               backref='perantau_kabupaten',
                               lazy=True)

    def __repr__(self):
        return '<Regency {}>'.format(self.name)


class Districts(db.Model):
    __tablename__ = "districts"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    regency_id = db.Column(db.Integer,
                           db.ForeignKey('regencies.id',
                                         ondelete="CASCADE",
                                         onupdate="CASCADE"),
                           nullable=False)
    districts = db.relationship('Villages', backref='districts', lazy=True)
    perantau = db.relationship('CovidPerantau',
                               backref='perantau_asal_kecamatan',
                               lazy=True)
    detektor = db.relationship('DeteksiSakit',
                               backref='lapor_sakit_kecamatan',
                               lazy=True)

    def __repr__(self):
        return '<District {}>'.format(self.name)


class Villages(db.Model):
    __tablename__ = "villages"
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    district_id = db.Column(db.Integer,
                            db.ForeignKey('districts.id',
                                          ondelete="CASCADE",
                                          onupdate="CASCADE"),
                            nullable=False)
    detektor = db.relationship('DeteksiSakit',
                               backref='lapor_sakit_desa',
                               lazy=True)
    perantau = db.relationship('CovidPerantau',
                               backref='perantau_asal_desa',
                               lazy=True)

    def __repr__(self):
        return '<Village {}>'.format(self.name)
