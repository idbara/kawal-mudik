from .. import db,ma
from .mixins import TimestampMixin
from app.search_mixins import SearchableMixin


class CovidPerantau(TimestampMixin, SearchableMixin, db.Model):
    __tablename__ = "covid_perantau"
    __searchable__ = ['nik']
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    first_name = db.Column(db.String(64), index=True, nullable=False)
    last_name = db.Column(db.String(64))
    nik = db.Column(db.BigInteger)
    tgl_lahir = db.Column(db.Date)
    jk = db.Column(db.String(1))
    shdk = db.Column(db.String(2))
    jml_ak_dan_pemudik = db.Column(db.Integer)
    tujuan_kecamatan = db.Column(
        db.Integer,
        db.ForeignKey('districts.id', ondelete="CASCADE", onupdate="CASCADE"))
    tujuan_desa = db.Column(
        db.BigInteger,
        db.ForeignKey('villages.id', ondelete="CASCADE", onupdate="CASCADE"))
    alamat = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(64))
    telp = db.Column(db.String(100))
    asal_negara = db.Column(
        db.String,
        db.ForeignKey('country.id', ondelete="CASCADE", onupdate="CASCADE"))
    asal_provinsi = db.Column(
        db.Integer,
        db.ForeignKey('provinces.id', ondelete="CASCADE", onupdate="CASCADE"))
    asal_kabupaten = db.Column(
        db.Integer,
        db.ForeignKey('regencies.id', ondelete="CASCADE", onupdate="CASCADE"))
    tgl_mudik = db.Column(db.Date, nullable=False)
    alasan_mudik = db.Column(db.String(100))
    alasan_mudik_lainnya = db.Column(db.String(100))
    demam = db.Column(db.Boolean, default=False, nullable=False)
    batuk_pilek = db.Column(db.Boolean, default=False, nullable=False)
    sakit_tenggorokan = db.Column(db.Boolean, default=False, nullable=False)
    sesak_napas = db.Column(db.Boolean, default=False, nullable=False)
    confirmed = db.Column(db.Boolean, default=False, nullable=False)

    def full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    def pulang(self):
        self.confirmed = True
        db.session.add(self)
        db.session.commit()
        return True

    def __repr__(self):
        return '<Perantau \'%s\'>' % self.full_name()



class CovidDeteksiMandiri(TimestampMixin, db.Model):
    __tablename__ = "covid_deteksi_mandiri"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    demam = db.Column(db.Boolean, nullable=False)
    batuk_pilek = db.Column(db.Boolean, nullable=False)
    sakit_tenggorokan = db.Column(db.Boolean, nullable=False)
    sesak_napas = db.Column(db.Boolean, nullable=False)
    penyakit_14hari = db.Column(db.Boolean, nullable=False)
    riwayat_kontak_penderita = db.Column(db.Boolean, nullable=False)
    riwayat_prjalan_luar = db.Column(db.Boolean, nullable=False)
    riwayat_perjalanan_lokal = db.Column(db.Boolean, nullable=False)


class DeteksiSakit(TimestampMixin, db.Model):
    __tablename__ = "covid_deteksi_sakit"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    first_name = db.Column(db.String(64), index=True, nullable=False)
    last_name = db.Column(db.String(64), index=True, nullable=False)
    kecamatan = db.Column(
        db.Integer,
        db.ForeignKey('districts.id', ondelete="CASCADE", onupdate="CASCADE"))
    desa = db.Column(
        db.BigInteger,
        db.ForeignKey('villages.id', ondelete="CASCADE", onupdate="CASCADE"))
    alamat = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(64), unique=True, index=True, nullable=False)
    telp = db.Column(db.String(100), unique=True, nullable=False)
    confirmed = db.Column(db.Boolean, default=False, nullable=False)

    def sudah_ditangani(self):
        self.confirmed = True
        db.session.add(self)
        db.session.commit()
        return True

    def full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    def __repr__(self):
        return '<Perantau \'%s\'>' % self.full_name()


class Posko(TimestampMixin, db.Model):
    __tablename__ = "covid_posko_kesehatan"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(64), index=True, nullable=False)
    alamat = db.Column(db.String(100), nullable=False)
    telp = db.Column(db.String(100), unique=True, nullable=False)

    def __repr__(self):
        return '<Nama Posko {}>'.format(self.name)


class RumahSakit(TimestampMixin, db.Model):
    __tablename__ = "covid_rumahsakit"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(64), index=True, nullable=False)
    alamat = db.Column(db.String(100), nullable=False)
    telp = db.Column(db.String(100), unique=True, nullable=False)

    def __repr__(self):
        return '<Nama Rumah Sakit {}>'.format(self.name)


class Puskesmas(TimestampMixin, db.Model):
    __tablename__ = "covid_puskesmas"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(64), index=True, nullable=False)
    alamat = db.Column(db.String(100), nullable=False)
    telp = db.Column(db.String(100), unique=True, nullable=False)

    def __repr__(self):
        return '<Nama Puskesmas {}>'.format(self.name)


class DataODP(TimestampMixin, db.Model):
    __tablename__ = "covid_odp"
    id = db.Column(db.Integer, primary_key=True)
    odp_pemantauan = db.Column(db.Integer, nullable=False)
    odp_selesai = db.Column(db.Integer, nullable=False)

    def total(self):
        return '<Data ODP {}>'.format(self.odp_pemantauan + self.odp_selesai)


class DataPDP(TimestampMixin, db.Model):
    __tablename__ = "covid_pdp"
    id = db.Column(db.Integer, primary_key=True)
    pdp_pengawasan = db.Column(db.Integer, nullable=False)
    pdp_selesai = db.Column(db.Integer, nullable=False)
    pdp_meninggal_negatif = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<Data PDP {}>'.format(self.pdp_total)


class DataPositif(TimestampMixin, db.Model):
    __tablename__ = "covid_positif"
    id = db.Column(db.Integer, primary_key=True)
    positif_dirawat = db.Column(db.Integer, nullable=False)
    positif_sembuh = db.Column(db.Integer, nullable=False)
    positif_meninggal = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<Data Positif {}>'.format(self.positif_total)


class EmailNotify(TimestampMixin, db.Model):
    __tablename__ = "covid_notify"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True, nullable=False)

    def __repr__(self):
        return '<Notifikasi {}>'.format(self.email)
