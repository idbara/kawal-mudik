from flask import (Blueprint, render_template, flash, redirect, url_for, g,
                   request, current_app)
from flask_rq import get_queue
from flask_babel import _
from app.covid19.forms import (CovidPerantauForm, CovidPerantauLuarNegeriForm,
                               ScreeningForm, DeteksiSakitForm, SearchForm)
from app.models import (CovidPerantau, CovidDeteksiMandiri, DeteksiSakit,
                        Posko, RumahSakit, Puskesmas, DataODP, DataPDP,
                        DataPositif)
from app import db
from app.email import send_email
from config import Config
from datetime import date

covid19 = Blueprint('covid19', __name__)


@covid19.before_request
def before_request():
    db.session.commit()
    g.search_form = SearchForm()


@covid19.route('/lapor-mudik', methods=['GET', 'POST'])
def LaporMudik():
    """Tambah Lapor Mudik Masuk Desa"""
    title = "Input Data Pemudik Dalam Negeri"
    description = "Kawal Mudik"
    form = CovidPerantauForm()
    if form.validate_on_submit():
        q = CovidPerantau(first_name=form.first_name.data,
                          last_name=form.last_name.data,
                          nik=form.nik.data,
                          tgl_lahir=form.tgl_lahir.data,
                          jk=form.jk.data,
                          shdk=form.shdk.data,
                          jml_ak_dan_pemudik=form.jml_ak_dan_pemudik.data,
                          tujuan_kecamatan=form.tujuan_kecamatan.data,
                          tujuan_desa=form.tujuan_desa.data,
                          alamat=form.alamat.data,
                          email=form.email.data,
                          telp=form.telp.data,
                          asal_provinsi=form.asal_provinsi.data,
                          asal_kabupaten=form.asal_kabupaten.data,
                          alasan_mudik=form.alasan_mudik.data,
                          alasan_mudik_lainnya=form.alasan_mudik_lainnya.data,
                          tgl_mudik=form.tgl_mudik.data,
                          demam=form.demam.data,
                          batuk_pilek=form.batuk_pilek.data,
                          sakit_tenggorokan=form.sakit_tenggorokan.data,
                          sesak_napas=form.sesak_napas.data)
        db.session.add(q)
        print(q)
        db.session.commit()
        if q.email == '':
            flash('Terima kasih sudah membuat laporan !!', 'success')
            return redirect(url_for('covid19.LaporMudik'))
        else:
            get_queue().enqueue(send_email,
                                recipient=q.email,
                                subject='Laporan Mudik Diterima',
                                template='account/email/lapor_mudik',
                                q=q)
            flash(
                'Laporan Berhasil, Pemberitahuan akan di \
                        kirimkan melalui email {}.'.format(q.email), 'success')
            return redirect(url_for('covid19.LaporMudik'))
    return render_template('covid19/index.html',
                           form=form,
                           title=title,
                           description=description)


@covid19.route('/lapor-mudik/luar-negeri', methods=['GET', 'POST'])
def LaporMudikLuarNegeri():
    """Tambah Lapor Mudik Masuk Desa dari Luar Negeri"""
    title = "Input Data Pemudik Luar Negeri"
    description = "Kawal Mudik"
    form = CovidPerantauLuarNegeriForm()
    if form.validate_on_submit():
        q = CovidPerantau(first_name=form.first_name.data,
                          last_name=form.last_name.data,
                          nik=form.nik.data,
                          tgl_lahir=form.tgl_lahir.data,
                          jk=form.jk.data,
                          shdk=form.shdk.data,
                          jml_ak_dan_pemudik=form.jml_ak_dan_pemudik.data,
                          tujuan_kecamatan=form.tujuan_kecamatan.data,
                          tujuan_desa=form.tujuan_desa.data,
                          alamat=form.alamat.data,
                          email=form.email.data,
                          telp=form.telp.data,
                          asal_negara=form.asal_negara.data,
                          alasan_mudik=form.alasan_mudik.data,
                          alasan_mudik_lainnya=form.alasan_mudik_lainnya.data,
                          tgl_mudik=form.tgl_mudik.data,
                          demam=form.demam.data,
                          batuk_pilek=form.batuk_pilek.data,
                          sakit_tenggorokan=form.sakit_tenggorokan.data,
                          sesak_napas=form.sesak_napas.data)
        db.session.add(q)
        db.session.commit()
        if q.email == '':
            flash('Terima kasih sudah membuat laporan !!', 'success')
            return redirect(url_for('covid19.LaporMudik'))
        else:
            get_queue().enqueue(send_email,
                                recipient=q.email,
                                subject='Laporan Mudik Diterima',
                                template='account/email/lapor_mudik',
                                q=q)
            flash(
                'Laporan Berhasil, Pemberitahuan akan di \
                        kirimkan melalui email {}.'.format(q.email), 'success')
            return redirect(url_for('covid19.LaporMudik'))
    return render_template('covid19/index.html',
                           form=form,
                           title=title,
                           description=description)


@covid19.route('/screening', methods=['GET', 'POST'])
def Screening():
    """Tambah Data Screening atau Deteksi Mandiri"""
    title = "Deteksi Mandiri"
    description = "Kawal Mudik"
    form = ScreeningForm()
    if form.validate_on_submit():
        q = CovidDeteksiMandiri(
            demam=form.demam.data,
            batuk_pilek=form.batuk_pilek.data,
            sakit_tenggorokan=form.sakit_tenggorokan.data,
            sesak_napas=form.sesak_napas.data,
            penyakit_14hari=form.penyakit_14hari.data,
            riwayat_kontak_penderita=form.riwayat_kontak_penderita.data,
            riwayat_prjalan_luar=form.riwayat_prjalan_luar.data,
            riwayat_perjalanan_lokal=form.riwayat_perjalanan_lokal.data)
        print(form.demam.data)
        if (form.demam.data or form.batuk_pilek.data
                or form.sakit_tenggorokan.data or form.sesak_napas.data or
                form.penyakit_14hari.data or form.riwayat_kontak_penderita.data
                or form.riwayat_prjalan_luar.data
                or form.riwayat_perjalanan_lokal.data) is True:
            db.session.add(q)
            db.session.commit()
            return redirect(url_for('covid19.ScreeningSakit'))
        else:
            return redirect(url_for('covid19.ScreeningSehat'))
    return render_template('covid19/screening.html',
                           title=title,
                           form=form,
                           description=description)


@covid19.route('/screening-sehat', methods=['GET', 'POST'])
def ScreeningSehat():
    """Informasi Deteksi Mandiri Sehat"""
    title = "Status Sehat"
    description = "Kawal Mudik"
    return render_template('covid19/screening_sehat.html',
                           title=title,
                           description=description)


@covid19.route('/screening-sakit', methods=['GET', 'POST'])
def ScreeningSakit():
    """Tambah Data Hasil Screening Sakit"""
    title = "Lapor Saya Sakit"
    description = "Kawal Mudik"
    form = DeteksiSakitForm()
    if form.validate_on_submit():
        q = DeteksiSakit(first_name=form.first_name.data,
                         last_name=form.last_name.data,
                         kecamatan=form.kecamatan.data,
                         desa=form.desa.data,
                         alamat=form.alamat.data,
                         email=form.email.data,
                         telp=form.telp.data)
        db.session.add(q)
        db.session.commit()
        get_queue().enqueue(send_email,
                            recipient=Config.ADMIN_EMAIL,
                            subject='Pemberitahuan Laporan Masyarakat Sakit',
                            template='account/email/lapor_admin_sakit')
        get_queue().enqueue(send_email,
                            recipient=q.email,
                            subject='Laporan Sakit Diterima',
                            template='account/email/lapor_sakit',
                            q=q)
        flash(
            'Laporan Berhasil, Pemberitahuan akan di \
                    kirimkan melalui email {}.'.format(q.email), 'success')
        return redirect(url_for('covid19.LaporMudik'))
        flash('Data berhasil di tambahkan', 'success')
    return render_template('covid19/screening_sakit.html',
                           form=form,
                           title=title,
                           description=description)


@covid19.route('/fasilitas-kesehatan', methods=['GET', 'POST'])
def FasilitasKesehatan():
    """Fasilitas Kesehatan Posko, Rumah Sakit dan Puskesmas"""
    title = "Fasilitas Kesehatan"
    description = "Kawal Mudik"
    posko = Posko.query.all()
    rs = RumahSakit.query.all()
    puskesmas = Puskesmas.query.all()
    return render_template('covid19/fasilitas_kesehatan.html',
                           title=title,
                           description=description,
                           posko=posko,
                           rs=rs,
                           puskesmas=puskesmas)


"""Statistik Covid-19 Kabupaten"""
@covid19.route('/publik/statistik-corona')
def StatistikCoronaKabupaten():
    odp = DataODP.query.order_by(DataODP.id.desc()).first()
    pdp = DataPDP.query.order_by(DataPDP.id.desc()).first()
    positif = DataPositif.query.order_by(DataPositif.id.desc()).first()
    return render_template('covid19/embed/statistik_kabupaten.html',
                           odp=odp,
                           pdp=pdp,
                           positif=positif)


"""Statistik Pemudik Desa"""
@covid19.route('/publik/statistik-pemudik/<int:village_id>')
def StatistikPemudikDesa(village_id):
    today = date.today()
    mudik_today = CovidPerantau.query.filter_by(
        tgl_mudik=today, tujuan_desa=village_id).count()
    total_perantau = CovidPerantau.query.filter_by(
        tujuan_desa=village_id).count()
    total_perantau_pulang = CovidPerantau.query.filter_by(
        tujuan_desa=village_id, confirmed=True).count()
    news_pemudik = CovidPerantau.query.filter_by(
        tujuan_desa=village_id).order_by(
            CovidPerantau.id.desc()).limit(10).all()
    return render_template('covid19/embed/statistik_pemudik_desa.html',
                           total_perantau=total_perantau,
                           total_perantau_pulang=total_perantau_pulang,
                           mudik_today=mudik_today,
                           news_pemudik=news_pemudik)


"""Statistik Masyarakat Sakit"""
@covid19.route('/publik/statistik-sakit/<int:village_id>')
def StatistikSakitDesa(village_id):
    total_sakit = DeteksiSakit.query.count()
    total_sakit_desa = DeteksiSakit.query.filter_by(desa=village_id).count()
    total_sakit_sudah_ditangani = DeteksiSakit.query.filter_by(
        desa=village_id, confirmed=True).count()
    news_laporsakit = DeteksiSakit.query.filter_by(desa=village_id).order_by(
        DeteksiSakit.id.desc()).limit(5).all()
    return render_template(
        'covid19/embed/statistik_sakit_desa.html',
        total_sakit_desa=total_sakit_desa,
        total_sakit_sudah_ditangani=total_sakit_sudah_ditangani,
        total_sakit=total_sakit,
        news_laporsakit=news_laporsakit)


"""Cari Pemudik"""
@covid19.route('/cek/pemudik')
def SearchPemudik():
    title = "Hasil Pencarian Data Pemudik"
    form = SearchForm()
    if not g.search_form.validate():
        return render_template('covid19/embed/cek_pemudik.html',
                               form=form,
                               page=_('Cari Data Pemudik'))
    page = request.args.get('page', 1, type=int)
    results, total = CovidPerantau.search(
        g.search_form.search.data, page,
        current_app.config['RESULTS_PER_PAGE'])
    next_url = url_for(
        'covid19.SearchPemudik', q=g.search_form.search.data,
        page=page + 1) \
        if total > page * current_app.config['RESULTS_PER_PAGE'] else None
    prev_url = url_for(
        'covid19.SearchPemudik', q=g.search_form.search.data,
        page=page - 1) \
        if page > 1 else None
    return render_template('covid19/embed/cek_pemudik.html',
                           form=form,
                           title=title,
                           page=_('Hasil Pencarian Data Pemudik'),
                           results=results,
                           total=total,
                           next_url=next_url,
                           prev_url=prev_url)
