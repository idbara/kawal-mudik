from flask import request
from flask_wtf import FlaskForm, RecaptchaField
from wtforms import ValidationError
from wtforms.fields import (StringField, SubmitField, SelectField, DateField,
                            IntegerField)
from wtforms.fields.html5 import EmailField
from wtforms.validators import (Email, InputRequired, Length, DataRequired,
                                Optional)
from app.models import (Country, Provinces, Regencies, Districts, Villages,
                        DeteksiSakit)


class SearchForm(FlaskForm):
    search = StringField('Masukan NIK', validators=[DataRequired()])
    submit = SubmitField('Search')

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)


class CovidPerantauForm(FlaskForm):
    first_name = StringField('Nama Depan',
                             validators=[InputRequired(),
                                         Length(1, 64)])
    last_name = StringField('Nama Belakang')
    nik = StringField('NIK', validators=[InputRequired()])
    tgl_lahir = DateField('Tanggal Lahir',
                          format='%Y/%m/%d',
                          validators=[Optional()])
    jk = SelectField('Jenis Kelamin ',
                     choices=[(0, '--Pilih Jenis Kelamin--'), (1, 'Laki-laki'),
                              (2, 'Perempuan')],
                     coerce=int)
    shdk = SelectField('Status Hubungan Dalam Keluarga',
                       choices=[(0, '--Pilih Salah Satu--'),
                                (1, 'Kepala Keluarga'), (2, 'Suami'),
                                (3, 'Istri'), (4, 'Anak'), (5, 'Menantu'),
                                (6, 'Cucu'), (7, 'Orang Tua'), (8, 'Mertua'),
                                (9, 'Famili Lain'), (10, 'Pembantu'),
                                (11, 'Lainnya')],
                       coerce=int)
    jml_ak_dan_pemudik = IntegerField('Jumlah Anggota Keluarga + Pemudik',
                                      validators=[InputRequired()],
                                      default=0)
    tujuan_kecamatan = SelectField('Kecamatan Tujuan:',
                                   validators=[DataRequired()],
                                   id='select_tujuankecamatan',
                                   coerce=int)
    tujuan_desa = SelectField('Desa/Kelurahan Tujuan:',
                              validators=[DataRequired()],
                              id='select_tujuandesa',
                              coerce=int)
    alamat = StringField(
        'Alamat Lengkap',
        validators=[InputRequired('Masukan Alamat !!'),
                    Length(1, 64)])
    email = EmailField('Email')
    telp = StringField('Nomor HP/Telp', validators=[InputRequired()])
    asal_provinsi = SelectField('Provinsi Asal:',
                                validators=[DataRequired()],
                                id='select_asalprovinsi',
                                coerce=int)
    asal_kabupaten = SelectField('Kabupaten Asal:',
                                 validators=[DataRequired()],
                                 id='select_asalkabupaten',
                                 coerce=int,
                                 default=0)
    alasan_mudik = SelectField('Alasan Mudik ',
                               choices=[
                                   (1, 'Kehilangan pekerjaan sementara/tetap'),
                                   (2, 'Usaha sepi/Bangkrut'),
                                   (3, 'Cuti Kerja/Liburan'), (4, 'Lainnya')
                               ],
                               coerce=int)
    alasan_mudik_lainnya = StringField('Alasan Mudik Lainnya')
    tgl_mudik = DateField('Tanggal Mudik',
                          format='%Y/%m/%d',
                          validators=[DataRequired('Masukan Mudik')])
    demam = SelectField('Demam',
                        choices=[(True, 'Ya'), (False, 'Tidak')],
                        default=False,
                        coerce=lambda x: x == 'True')
    batuk_pilek = SelectField('Batuk Pilek',
                              choices=[(True, 'Ya'), (False, 'Tidak')],
                              default=False,
                              coerce=lambda x: x == 'True')
    sakit_tenggorokan = SelectField('Sakit Tenggorokan ',
                                    choices=[(True, 'Ya'), (False, 'Tidak')],
                                    default=False,
                                    coerce=lambda x: x == 'True')
    sesak_napas = SelectField('Sesak Napas',
                              choices=[(True, 'Ya'), (False, 'Tidak')],
                              default=False,
                              coerce=lambda x: x == 'True')
    # recaptcha = RecaptchaField()
    submit = SubmitField('Lapor Sekarang')

    def validate_nik(form, field):
        if len(field.data) != 16:
            raise ValidationError('Masukan NIK dengan benar')

    def validate_jk(form, field):
        if field.data == 0:
            raise ValidationError('Pilih Jenis Kelamin')

    def validate_shdk(form, field):
        if field.data == 0:
            raise ValidationError('Pilih SHDK')

    def __init__(self, *args, **kwargs):
        super(CovidPerantauForm, self).__init__(*args, **kwargs)
        self.asal_provinsi.choices = [
            (row.id, row.name)
            for row in Provinces.query.order_by('name').all()
        ]
        self.asal_kabupaten.choices = [
            (row.id, row.name) for row in Regencies.query.filter_by(
                province_id=Provinces.id).order_by('name').all()
        ]
        self.tujuan_kecamatan.choices = [
            (row.id, row.name) for row in Districts.query.filter_by(
                regency_id=Regencies.id).order_by('name').all()
        ]
        self.tujuan_desa.choices = [
            (row.id, row.name) for row in Villages.query.filter_by(
                district_id=Districts.id).order_by('name').all()
        ]


class CovidPerantauLuarNegeriForm(FlaskForm):
    first_name = StringField('Nama Depan',
                             validators=[InputRequired(),
                                         Length(1, 64)])
    last_name = StringField('Nama Belakang')
    nik = StringField('NIK', validators=[InputRequired()])
    tgl_lahir = DateField('Tanggal Lahir',
                          format='%Y/%m/%d',
                          validators=[Optional()])
    jk = SelectField('Jenis Kelamin ',
                     choices=[(0, '--Pilih Jenis Kelamin--'), (1, 'Laki-laki'),
                              (2, 'Perempuan')],
                     coerce=int)
    shdk = SelectField('Status Hubungan Dalam Keluarga',
                       choices=[(0, '--Pilih Salah Satu--'),
                                (1, 'Kepala Keluarga'), (2, 'Suami'),
                                (3, 'Istri'), (4, 'Anak'), (5, 'Menantu'),
                                (6, 'Cucu'), (7, 'Orang Tua'), (8, 'Mertua'),
                                (9, 'Famili Lain'), (10, 'Pembantu'),
                                (11, 'Lainnya')],
                       coerce=int)
    jml_ak_dan_pemudik = IntegerField('Jumlah Anggota Keluarga + Pemudik',
                                      validators=[InputRequired()],
                                      default=0)
    tujuan_kecamatan = SelectField('Kecamatan Tujuan:',
                                   validators=[DataRequired()],
                                   id='select_tujuankecamatan',
                                   coerce=int)
    tujuan_desa = SelectField('Desa/Kelurahan Tujuan:',
                              validators=[DataRequired()],
                              id='select_tujuandesa',
                              coerce=int)
    alamat = StringField(
        'Alamat Lengkap',
        validators=[InputRequired('Masukan Alamat !!'),
                    Length(1, 64)])
    email = EmailField('Email')
    telp = StringField('Nomor HP/Telp', validators=[InputRequired()])
    asal_negara = SelectField('Negara Asal:', validators=[DataRequired()])
    alasan_mudik = SelectField('Alasan Mudik ',
                               choices=[
                                   (1, 'Kehilangan pekerjaan sementara/tetap'),
                                   (2, 'Usaha sepi/Bangkrut'),
                                   (3, 'Cuti Kerja/Liburan'), (4, 'Lainnya')
                               ],
                               coerce=int)
    alasan_mudik_lainnya = StringField('Alasan Mudik Lainnya')
    tgl_mudik = DateField('Tanggal Mudik',
                          format='%Y/%m/%d',
                          validators=[DataRequired('Masukan Mudik')])
    demam = SelectField('Demam',
                        choices=[(True, 'Ya'), (False, 'Tidak')],
                        default=False,
                        coerce=lambda x: x == 'True')
    batuk_pilek = SelectField('Batuk Pilek',
                              choices=[(True, 'Ya'), (False, 'Tidak')],
                              default=False,
                              coerce=lambda x: x == 'True')
    sakit_tenggorokan = SelectField('Sakit Tenggorokan ',
                                    choices=[(True, 'Ya'), (False, 'Tidak')],
                                    default=False,
                                    coerce=lambda x: x == 'True')
    sesak_napas = SelectField('Sesak Napas',
                              choices=[(True, 'Ya'), (False, 'Tidak')],
                              default=False,
                              coerce=lambda x: x == 'True')
    # recaptcha = RecaptchaField()
    submit = SubmitField('Lapor Sekarang')

    def validate_nik(form, field):
        if len(field.data) != 16:
            raise ValidationError('Masukan NIK dengan benar')

    def validate_jk(form, field):
        if field.data == 0:
            raise ValidationError('Pilih Jenis Kelamin')

    def validate_shdk(form, field):
        if field.data == 0:
            raise ValidationError('Pilih SHDK')

    def __init__(self, *args, **kwargs):
        super(CovidPerantauLuarNegeriForm, self).__init__(*args, **kwargs)
        self.asal_negara.choices = [
            (row.id, row.name) for row in Country.query.order_by('name').all()
        ]
        self.tujuan_kecamatan.choices = [
            (row.id, row.name) for row in Districts.query.filter_by(
                regency_id=Regencies.id).order_by('name').all()
        ]
        self.tujuan_desa.choices = [
            (row.id, row.name) for row in Villages.query.filter_by(
                district_id=Districts.id).order_by('name').all()
        ]


CHOICES = [(True, 'Ya'), (False, 'Tidak')]


class ScreeningForm(FlaskForm):
    demam = SelectField('Demam',
                        choices=CHOICES,
                        default=None,
                        coerce=lambda x: x == 'True')
    batuk_pilek = SelectField('Batuk Pilek',
                              choices=[(True, 'Ya'), (False, 'Tidak')],
                              coerce=lambda x: x == 'True')
    sakit_tenggorokan = SelectField('Sakit Tenggorokan ',
                                    choices=[(True, 'Ya'), (False, 'Tidak')],
                                    coerce=lambda x: x == 'True')
    sesak_napas = SelectField('Sesak Napas',
                              choices=[(True, 'Ya'), (False, 'Tidak')],
                              coerce=lambda x: x == 'True')
    penyakit_14hari = SelectField('Lama penyakit kurang dari 14 hari',
                                  choices=[(True, 'Ya'), (False, 'Tidak')],
                                  coerce=lambda x: x == 'True')
    riwayat_kontak_penderita = SelectField(
        'Memiliki riwayat kontak dengan penderita terkonfirmasi COVID-19 \
            atau probabel COVID-19',
        choices=[(True, 'Ya'), (False, 'Tidak')],
        coerce=lambda x: x == 'True')
    riwayat_prjalan_luar = SelectField(
        'Memiliki riwayat perjalanan atau tinggal diluar negeri yang melakukan \
            penularan lokal',
        choices=[(True, 'Ya'), (False, 'Tidak')],
        coerce=lambda x: x == 'True')
    riwayat_perjalanan_lokal = SelectField(
        'Memiliki riwayat perjalanan atau tinggal diarea \
            penularan lokal di Indonesia',
        choices=[(True, 'Ya'), (False, 'Tidak')],
        coerce=lambda x: x == 'True')
    # recaptcha = RecaptchaField()
    submit = SubmitField('Cek Hasil')


class DeteksiSakitForm(FlaskForm):
    first_name = StringField('Nama Depan',
                             validators=[InputRequired(),
                                         Length(1, 64)])
    last_name = StringField('Nama Belakang',
                            validators=[InputRequired(),
                                        Length(1, 64)])
    kecamatan = SelectField('Saya ada di Kecamatan:',
                            validators=[DataRequired()],
                            id='lokasi_kecamatan',
                            coerce=int)
    desa = SelectField('Saya ada di Desa:',
                       validators=[DataRequired()],
                       id='lokasi_desa',
                       coerce=int)
    alamat = StringField(
        'Alamat Lengkap',
        validators=[InputRequired('Masukan Alamat !!'),
                    Length(1, 64)])
    email = EmailField('Email',
                       validators=[
                           InputRequired(),
                           Length(1, 64),
                           Email(message='Masukan email dengan benar !!!')
                       ])
    telp = StringField(
        'Nomor HP/Telp',
        validators=[InputRequired('Masukan Nomor HP/Telp !!'),
                    Length(1, 64)])
    # recaptcha = RecaptchaField()
    submit = SubmitField('Lapor Sekarang')

    def validate_email(self, field):
        if DeteksiSakit.query.filter_by(email=field.data).first():
            raise ValidationError('Email sudah terdaftar !!')

    def validate_telp(self, field):
        if DeteksiSakit.query.filter_by(telp=field.data).first():
            raise ValidationError('Nomor HP sudah terdaftar !!')

    def __init__(self, *args, **kwargs):
        super(DeteksiSakitForm, self).__init__(*args, **kwargs)
        self.kecamatan.choices = [
            (row.id, row.name) for row in Districts.query.filter_by(
                regency_id=Regencies.id).order_by('name').all()
        ]
        self.desa.choices = [
            (row.id, row.name) for row in Villages.query.filter_by(
                district_id=Districts.id).order_by('name').all()
        ]
