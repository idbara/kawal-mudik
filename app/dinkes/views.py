from flask import Blueprint, render_template
from flask_login import login_required
from app.decorators import required_roles

dinkes = Blueprint('dinkes', __name__)


@dinkes.route('/')
@login_required
@required_roles('Dinkes')
def index():
    return render_template('dinkes/index.html')
