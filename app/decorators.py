from functools import wraps

from flask import abort
from flask_login import current_user


def required_roles(*roles):
    """Restrict a view to users with the given permission."""
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if current_user.get_role() not in roles:
                abort(403)
            return f(*args, **kwargs)

        return decorated_function

    return decorator
