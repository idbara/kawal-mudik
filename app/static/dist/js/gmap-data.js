/*Gmap Init*/
$(function () {
	"use strict";
	
	/* Map initialization js*/
	if( $('#maps_input').length > 0 ){	
		var settings = {
			zoom: 13,
			center: new google.maps.LatLng(-6.929645, 109.366425),
			mapTypeControl: false,
			scrollwheel: false,
			draggable: true,
			panControl:false,
			scaleControl: false,
			zoomControl: true,
			streetViewControl:false,
			navigationControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		    styles: [
				{
					"featureType": "landscape.natural.landcover",
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"visibility": "on"
						}
					]
				}
			]};		
			var map = new google.maps.Map(document.getElementById("maps_input"), settings);	
			google.maps.event.addDomListener(window, "resize", function() {
				var center = map.getCenter();
				google.maps.event.trigger(map, "resize");
				map.setCenter(center);
			});	

			

	        var poly = new google.maps.Polyline({
	          strokeColor: '#FF0000',
	          strokeOpacity: 1.0,
	          strokeWeight: 2
	        });
	        poly.setMap(map);
	        function removeLine() {
		        poly.setMap(null);
		    }

	        function savepolyline() {
	        	var datapolyline = poly.getPath();
	        	console.log(datapolyline);
	        }

	        // Add a listener for the click event
        	map.addListener('click', addLatLng);
        	// Handles click events on a map, and adds a new point to the Polyline.
        	function addLatLng(event) {
        		var path = poly.getPath();

				// Because path is an MVCArray, we can simply append a new coordinate
				// and it will automatically appear.
				console.log(path);
				path.push(event.latLng);

				// save to mysql using ajax
				$.ajax({
			      method: "POST",
			      url : "/sarpras/savepoly",
			      data: JSON.stringify( { "lat": event.latLng.lat(),"lng": event.latLng.lng() } ),
			      dataType: "json",
			      contentType: "application/json"
			    }).done(function(response){ //
			      console.log(response);
			    });
				// Add a new marker at the new plotted point on the polyline.
				/*var marker = new google.maps.Marker({
					position: event.latLng,
					title: '#' + path.getLength(),
					map: map
				});*/
			}
	}
	if( $('#maps').length > 0 ){	
		var settings = {
			zoom: 16,
			center: new google.maps.LatLng(37.772,-122.214),
			mapTypeControl: false,
			scrollwheel: false,
			draggable: true,
			panControl:false,
			scaleControl: false,
			zoomControl: true,
			streetViewControl:false,
			navigationControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		    styles: [
				{
					"featureType": "landscape.natural.landcover",
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"visibility": "on"
						}
					]
				}
			]};		
			var map = new google.maps.Map(document.getElementById("maps"), settings);	
			google.maps.event.addDomListener(window, "resize", function() {
				var center = map.getCenter();
				google.maps.event.trigger(map, "resize");
				map.setCenter(center);
			});	
			
			/*var infowindow = new google.maps.InfoWindow();	
			var companyPos = new google.maps.LatLng(-6.816379,109.535581);	
			var companyMarker = new google.maps.Marker({
				position: companyPos,
				map: map,
				title:"Our Office",
				zIndex: 3});	
			google.maps.event.addListener(companyMarker, 'click', function() {
				infowindow.open(map,companyMarker);

			});*/
			//-6.816379,109.535581
			var flightPlanCoordinates = [
	          {lat: 37.772, lng: -122.214},
	          {lat: 21.291, lng: -157.821},
	          {lat: -18.142, lng: 178.431},
	          {lat: -27.467, lng: 153.027}
	        ];
	        var flightPath = new google.maps.Polyline({
	          path: flightPlanCoordinates,
	          geodesic: true,
	          strokeColor: '#FF0000',
	          strokeOpacity: 1.0,
	          strokeWeight: 2
	        });

	        flightPath.setMap(map);
	}
	if( $('#map_canvas').length > 0 ){	
		var settings = {
			zoom: 16,
			center: new google.maps.LatLng(-6.816379,109.535581),
			mapTypeControl: false,
			scrollwheel: false,
			draggable: true,
			panControl:false,
			scaleControl: false,
			zoomControl: false,
			streetViewControl:false,
			navigationControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		    styles: [
				{
					"featureType": "landscape.natural.landcover",
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"visibility": "on"
						}
					]
				}
			]};		
			var map = new google.maps.Map(document.getElementById("map_canvas"), settings);	
			google.maps.event.addDomListener(window, "resize", function() {
				var center = map.getCenter();
				google.maps.event.trigger(map, "resize");
				map.setCenter(center);
			});	
			
			var infowindow = new google.maps.InfoWindow();	
			var companyPos = new google.maps.LatLng(-6.816379,109.535581);	
			var companyMarker = new google.maps.Marker({
				position: companyPos,
				map: map,
				title:"Our Office",
				zIndex: 3});	
			google.maps.event.addListener(companyMarker, 'click', function() {
				infowindow.open(map,companyMarker);
			});
	}
	if( $('#map_canvas_1').length > 0 ){	
	var settings = {
		zoom: 16,
		center: new google.maps.LatLng(43.270441,6.640888),
		mapTypeControl: false,
		scrollwheel: false,
		draggable: true,
		panControl:false,
		scaleControl: false,
		zoomControl: false,
		streetViewControl:false,
		navigationControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		styles: [
		{
			"featureType": "water",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#e9e9e9"
				},
				{
					"lightness": 17
				}
			]
		},
		{
			"featureType": "landscape",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#f5f5f5"
				},
				{
					"lightness": 20
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "geometry.fill",
			"stylers": [
				{
					"color": "#ffffff"
				},
				{
					"lightness": 17
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "geometry.stroke",
			"stylers": [
				{
					"color": "#ffffff"
				},
				{
					"lightness": 29
				},
				{
					"weight": 0.2
				}
			]
		},
		{
			"featureType": "road.arterial",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#ffffff"
				},
				{
					"lightness": 18
				}
			]
		},
		{
			"featureType": "road.local",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#ffffff"
				},
				{
					"lightness": 16
				}
			]
		},
		{
			"featureType": "poi",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#f5f5f5"
				},
				{
					"lightness": 21
				}
			]
		},
		{
			"featureType": "poi.park",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#dedede"
				},
				{
					"lightness": 21
				}
			]
		},
		{
			"elementType": "labels.text.stroke",
			"stylers": [
				{
					"visibility": "on"
				},
				{
					"color": "#ffffff"
				},
				{
					"lightness": 16
				}
			]
		},
		{
			"elementType": "labels.text.fill",
			"stylers": [
				{
					"saturation": 36
				},
				{
					"color": "#333333"
				},
				{
					"lightness": 40
				}
			]
		},
		{
			"elementType": "labels.icon",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "transit",
			"elementType": "geometry",
			"stylers": [
				{
					"color": "#f2f2f2"
				},
				{
					"lightness": 19
				}
			]
		},
		{
			"featureType": "administrative",
			"elementType": "geometry.fill",
			"stylers": [
				{
					"color": "#fefefe"
				},
				{
					"lightness": 20
				}
			]
		},
		{
			"featureType": "administrative",
			"elementType": "geometry.stroke",
			"stylers": [
				{
					"color": "#fefefe"
				},
				{
					"lightness": 17
				},
				{
					"weight": 1.2
				}
			]
		}
	]};		
	var map = new google.maps.Map(document.getElementById("map_canvas_1"), settings);	
	google.maps.event.addDomListener(window, "resize", function() {
		var center = map.getCenter();
		google.maps.event.trigger(map, "resize");
		map.setCenter(center);
	});	
	
	var infowindow = new google.maps.InfoWindow();	
	var companyPos = new google.maps.LatLng(43.270441,6.640888);	
	var companyMarker = new google.maps.Marker({
		position: companyPos,
		map: map,
		title:"Our Office",
		zIndex: 3});	
	google.maps.event.addListener(companyMarker, 'click', function() {
		infowindow.open(map,companyMarker);
	});
}
	if( $('#map_canvas_2').length > 0 ){	
	var settings = {
		zoom: 16,
		center: new google.maps.LatLng(43.270441,6.640888),
		mapTypeControl: false,
		scrollwheel: false,
		draggable: true,
		panControl:false,
		scaleControl: false,
		zoomControl: false,
		streetViewControl:false,
		navigationControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		 styles: [
		{
			"featureType": "administrative",
			"elementType": "labels.text.fill",
			"stylers": [
				{
					"color": "#444444"
				}
			]
		},
		{
			"featureType": "landscape",
			"elementType": "all",
			"stylers": [
				{
					"color": "#f2f2f2"
				},
				{
					"visibility": "on"
				}
			]
		},
		{
			"featureType": "landscape.natural",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "on"
				}
			]
		},
		{
			"featureType": "poi",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "all",
			"stylers": [
				{
					"saturation": -100
				},
				{
					"lightness": 45
				}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "road.arterial",
			"elementType": "labels.icon",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "transit",
			"elementType": "all",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "water",
			"elementType": "all",
			"stylers": [
				{
					"color": "#68ebb5"
				},
				{
					"visibility": "on"
				}
			]
		}
	]};		
	var map = new google.maps.Map(document.getElementById("map_canvas_2"), settings);	
	google.maps.event.addDomListener(window, "resize", function() {
		var center = map.getCenter();
		google.maps.event.trigger(map, "resize");
		map.setCenter(center);
	});	
	var contentString = '<div id="content-map-marker" style="text-align:left; padding-top:10px; padding-left:10px">'+
		'<div id="siteNotice">'+
		'</div>'+
		'<h6 id="firstHeading" class="firstHeading" style=" margin-bottom:0px;"><strong>Hello Friend!</strong></h4>'+
		'<div id="bodyContent">'+
		'<p style="font-family: Varela Round; color:#adadad; font-size:13px; margin-bottom:10px">Here we are. Come to drink a coffee!</p>'+
		'</div>'+
		'</div>';
	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});	
	
	var companyPos = new google.maps.LatLng(43.270441,6.640888);	
	var companyMarker = new google.maps.Marker({
		position: companyPos,
		map: map,
		title:"Our Office",
		zIndex: 3});	
	google.maps.event.addListener(companyMarker, 'click', function() {
		infowindow.open(map,companyMarker);
	});
}
});